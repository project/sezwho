<?php

/**
* @package SezWho
* @copyright (c) 2008 sezwho.com
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/


require_once($sz_plugin_path.'/engine/sz_generic_platform_wrapper.php');


/**
* platform-specific-wrapper must implement all abstract methods decalared in sz_generic_platform_wrapper class.
* It can also override any method defined in the parent class and define new methods to meet platform specific requirements. 
* 
* @package SezWho
*/
class dr_wrapper extends sz_generic_platform_wrapper
{
	//constructor
	function dr_wrapper()
	{
		//CONTRACT: platform-specific-wrapper::_construct must call parent::__construct($this);
		parent::sz_generic_platform_wrapper($this);
	}

/*================= BEGINING OF IMPLEMENTATION OF ABSTRACT FUNCTIONS ===================
======================================================================================*/
	/**
	* Returns value for the key
	*
	* @param string $key
	* @return string value
	*/
	function sz_get_key_value($key)
	{
		return variable_get($key,'');
	}
	
	
	/**
	* Sets value for the key
	*
	* @param string $key
	* @param string $value
	*/
	function sz_set_key_value($key, $value)
	{
		 variable_set($key, $value);
	}

	
	/**
	* Returns blog title
	*
	* @param int $blog_id
	* @return string blog title
	*/
	function sz_get_blog_title($blog_id=0) {
		$blog_title	= $this->sz_get_key_value('site_name');
		if ($blog_id!=0) {
			$blog_title	.= ' ('.$this->sz_get_username_by_id($blog_id). "\'s blog)";
		}
		return $blog_title;
	}
	
	
	/**
	* Returns blog subject
	*
	* @param int $blog_id
	* @return string blog subject
	*/
	function sz_get_blog_subject($blog_id=0) {
		return '';
	}
	
	
	/**
	* Returns blog url
	*
	* @param int $blog_id
	* @return string blog url
	*/
	function sz_get_blog_url($blog_id=0) {
		global $sz_site_url;
		$blog_url	= $sz_site_url;
		if ($blog_id!=0) {
			$blog_url	.= '?q=/blog/'.$blog_id;
		}
		return $blog_url;
	}
	
	
	/**
	* Returns the blog_id of the content
	* 
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return int id of the blog that contains the content
	*/
	function sz_get_content_blog_id(&$content)
	{
		$content_blog_id=isset($content['content_blog_id'])?$content['content_blog_id']:'';

		if($content_blog_id=='' || $content_blog_id==null)
		{
			$content_type=$this->sz_get_content_type($content);
			if('P'==$content_type)
			{
				if ('blog'==$content['type'])
				{
					$content['content_blog_id']=$content['uid'];
				}
				else
				{
					$content['content_blog_id']	=0;
				}
			}
			elseif ('C'==$content_type)
			{
				$context_id	= $this->sz_get_context_id($content);

				$sql 		= "SELECT type, uid FROM node WHERE nid = '$context_id'";
				$sql_result	= $this->sz_execute_select_query($sql);
				
				$context['content_id']		= $context_id;
				$context['content_dr_type']	= 'node';
				$context['type']			= $sql_result[0]['type'];
				$context['uid']				= $sql_result[0]['uid'];

				$content['content_blog_id']	= $this->sz_get_content_blog_id($context);
			}
		}
		return $content['content_blog_id'];
	}

	
	/**
	* Returns approved status of the content as in platform
	* 
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return boolean approved status
	*/
	function sz_is_content_approved(&$content) {
		return '1';
	}
	
	
	/**
	* Returns content type
	*
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return string
	*/
	function sz_get_content_type(&$content)
	{
		$content_type=isset($content['content_type'])?$content['content_type']:'';

		if ($content_type=='' || $content_type==null) {
			if('node'==$content['content_dr_type'])
			{
				$content['content_type']='P';
			}
			elseif('comment'==$content['content_dr_type'])
			{
				$content['content_type']='C';
			}
		}
		
		return $content['content_type'];
	}
	
	
	/**
	* Returns id of the content wrt platform
	* 
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return int id of the content wrt platform
	*/
	function sz_get_content_id(&$content)
	{
		$content_id=isset($content['content_id'])?$content['content_id']:'';
		
		if ($content_id=='' || $content_id==null) {
			$content_type=$this->sz_get_content_type($content);
			if ('P'==$content_type) {
				$content['content_id']=$content['nid'];
			}
			elseif ('C'==$content_type) {
				$content['content_id']=$content['cid'];
			}
		}
		return $content['content_id'];
		
	}

	
	/**
	* Returns the creation date of the content
	* 
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return string creation date of the content wrt platform
	*/

	function sz_get_content_creation_date(&$content)
	{
		$content_creation_date=isset($content['content_creation_date'])?$content['content_creation_date']:'';
		
		return $content['content_creation_date'];
	}
	
	
	/**
	* Returns subject/title of the content
	* 
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return string subject/title of the content
	*/
	function sz_get_content_title(&$content)
	{
		$content_title	= isset($content['content_title'])?$content['content_title']:'';
		if($content_title=='' || $content_title==null)
		{
			$content_type	= $this->sz_get_content_type($content);
			if('P'==$content_type)
			{
			$content['content_title']		= $content['title'];
			}
			elseif ('C'==$content_type)
			{
				$content['content_title']	= $content['subject'];
			}
		}
		return $content['content_title'];
	}
	
	
	/**
	* Returns the introduction of the content
	* 
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return string introduction of the content (preferably first 100 characters of the body of the content)
	*/
	function sz_get_content_intro(&$content)
	{
		$content_intro	= isset($content['content_intro'])?$content['content_intro']:'';
		
		if($content_intro=='' || $content_intro==null)
		{
			$content_type	= $this->sz_get_content_type($content);
			if('P'==$content_type)
			{
			$content['content_intro']		= $content['body'];
			}
			elseif ('C'==$content_type)
			{
				$content['content_intro']	= $content['comment'];
			}
		}
		
		return $content['content_intro'];
		
	}
	
	
	/**
	* Returns the url of the content
	* 
	* @param mixed $content either contains only contentid & contextid as $content['content_id'], $content['context_id'] or has all the fields selected in the
	* function get_query_to_fetch_unsynchroinzed_comments or similar type as the param passed to post_content
	* @return string introduction of the content (preferably first 100 characters of the body of the content)
	*/
	function sz_get_content_url(&$content)
	{
		global $sz_site_url;
		
		$content_url	= isset($content['content_url'])?$content['content_url']:'';
		if($content_url=='' || $content_url==null)
		{
			$content_id	= $this->sz_get_content_id($content);
			
			$content_type	= $this->sz_get_content_type($content);
			if('P'==$content_type)
			{
				$content['content_url']	= "$sz_site_url/?q=node/$content_id";
			}
			elseif ('C'==$content_type)
			{
				$context_id	= $this->sz_get_context_id($content);
				$content['content_url']	= "$sz_site_url/?q=node/$context_id#comment-$content_id";				
			}
		}
		return $content['content_url'];
	}
	
	
	/**
	* Returns the name of the author of the content
	* 
	* @param mixed $content either contains only contentid & contextid as $content['contentid'], $content['contextid'] or similar type as the param passed to
	* post_content
	* @return string name of the author of the content
	*/
	function sz_get_content_author_name(&$content)
	{
		$content_author_name	= isset($content['content_author_name'])?$content['content_author_name']:'';
		
		if($content_author_name=='' || $content_author_name==null)
		{
			$content['content_author_name']	= $content['name'];
		}
		
		return $content['content_author_name'];
	}
	
	
	/**
	* Returns the email of the author of the content
	* 
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return email of the author of the content
	*/
	function sz_get_content_author_email(&$content)
	{
		$content_author_email	= isset($content['content_author_email'])?$content['content_author_email']:'';
		
		if($content_author_email=='' || $content_author_email==null)
		{
			$content['content_author_email']		= $content['mail'];
			
			if($content['content_author_email']=='' || $content['content_author_email']==null)
			{
				$content['content_author_name']		= $this->sz_get_content_author_name($content);
				$sql	= "SELECT mail FROM users WHERE name='{$content['content_author_name']}'";
				$content['content_author_email']	= $this->sz_get_column_value_in_first_row_for_query($sql,'mail');
			}
		}

		return $content['content_author_email'];
	}

	
	/**
	* Returns the url of the author of the content
	* 
	* @param either contains only contentid & contextid as $content['contentid'], $content['contextid'] or similar type as the param passed to
	* post_content
	* @return url of the author of the content
	*/
	function sz_get_content_author_url(&$content)
	{
		return '';
	}
	
	
	/**
	* Returns the id of the context in the platform
	* 
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return int id of the context in the platform
	*/
	function sz_get_context_id(&$content)
	{
		$context_id=isset($content['context_id'])?$content['context_id']:'';
		
		if ($context_id=='' || $context_id==null) {
			$content_type=$this->sz_get_content_type($content);
			if ('P'==$content_type) {
				$content['context_id']	= -1;
			}
			elseif ('C'==$content_type) {
				$content['context_id']	= $content['nid'];
			}
		}
		return $content['context_id'];
	}
	
	
	/**
	* Returns the subject/title of the context
	* 
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return string subject/title of the context
	*/
	function sz_get_context_title(&$content)
	{
		$context_title	= isset($content['context_title'])?$content['context_title']:'';
		if($context_title=='' || $context_title==null)
		{
			$context_id	= $this->sz_get_context_id($content);
			$context	= get_object_vars(node_load($content['context_id']));
			$content['context_title']	= $context['title'];
			//since node is loaded it is a good opportunity to populate other attributes as well
			$content['context_intro']	= $context['body'];			
		}
		return $content['context_title'];
	}
	
	
	/**
	* Returns intro of the context (preferably first 100 characters of the body of the context)
	* 
	* @param either has all the fields selected in the function get_query_to_fetch_unsynchroinzed_comments or similar type as the param passed to
	* post_content
	* @return intro of the context (preferably first 100 characters of the body of the context)
	*/
	function sz_get_context_intro(&$content)
	{
		$context_intro	= isset($content['context_intro'])?$content['context_intro']:'';
		if($context_intro=='' || $context_intro==null)
		{
			$context_id	= $this->sz_get_context_id($content);
			$context	= get_object_vars(node_load($content['context_id']));
			$content['context_intro']	= $context['body'];
		}
		return $content['context_intro'];		
	}
		
	
	/**
	* Returns the url of the context
	*
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return string context url
	*/
	function sz_get_context_url(&$content)
	{
		global $sz_site_url;
		
		$context_url	= isset($content['context_url'])?$content['context_url']:'';
		
		if($context_url=='' || $context_url==nul)
		{
			$context_id	= $this->sz_get_context_id($content);
			$content['context_url']	= "$sz_site_url/?q=node/$context_id";
		}
		
		return $content['context_url'];
	}
	
	
	/**
	* Returns the context author name
	*
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return string context author name
	*/
	function sz_get_context_author_name(&$content){}
	
	
	/**
	* Returns the context author email
	*
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return string context author email
	*/
	function sz_get_context_author_email(&$content){}
	
	

	/**
	* Returns the qurey used to fetch unsynchronized platform content
	*
	* @param int $blog_id
	* @return string synchronization query
	*/
	function sz_get_unsynchroinzed_content_fetching_query($blog_id, $unsynchroinzed_content_fetching_query_param)
	{
		global $sz_site_url;
		
		if('posts'==$unsynchroinzed_content_fetching_query_param)
		{
			if ($blog_id == 0)
			{
				$sql	= "	SELECT
								0 												AS content_blog_id, 
								-1												AS context_id,
								'P' 											AS content_type,
								dr_nodes.nid 									AS content_id, 
								dr_nodes.title									AS content_title,
								dr_node_revisions.body							AS content_intro,
								CONCAT('$sz_site_url/?q=node/',dr_nodes.nid)	AS content_url,
								FROM_UNIXTIME(dr_nodes.created)					AS content_creation_date,
								dr_users.mail									AS content_author_email 
							
							FROM users dr_users, node_revisions dr_node_revisions, node dr_nodes
							LEFT JOIN sz_comment sz_content_table
								ON dr_nodes.nid=sz_content_table.comment_id 
							WHERE dr_nodes.uid=dr_users.uid 
								AND dr_nodes.status='1' 
								AND dr_nodes.nid=dr_node_revisions.nid 
								AND dr_nodes.type!='blog' 
								AND (sz_content_table.posting_id IS NULL 
									OR (sz_content_table.posting_id!=-1
										AND sz_content_table.comment_id NOT IN (SELECT comment_id FROM sz_comment WHERE posting_id=-1)
										)
									)";
			}
						
			else
			{
				$sql	= "	SELECT
								dr_nodes.uid									AS content_blog_id, 
								-1												AS context_id,
								'P' 											AS content_type,
								dr_nodes.nid 									AS content_id, 
								dr_nodes.title									AS content_title,
								dr_node_revisions.body							AS content_intro,
								CONCAT('$sz_site_url/?q=node/',dr_nodes.nid)	AS content_url,
								FROM_UNIXTIME(dr_nodes.created)					AS content_creation_date,
								dr_users.mail									AS content_author_email 
							
							FROM users dr_users, node_revisions dr_node_revisions, node dr_nodes
							LEFT JOIN sz_comment sz_content_table
								ON dr_nodes.nid=sz_content_table.comment_id 
							WHERE dr_nodes.uid=dr_users.uid 
								AND dr_nodes.status='1' 
								AND dr_nodes.nid=dr_node_revisions.nid 
								AND dr_nodes.uid=$blog_id								
								AND dr_nodes.type='blog' 
								AND (sz_content_table.posting_id IS NULL 
									OR (sz_content_table.posting_id!=-1
										AND sz_content_table.comment_id NOT IN (SELECT comment_id FROM sz_comment WHERE posting_id=-1)
										)
									)";
				
			}
		}
		elseif('comments'==$unsynchroinzed_content_fetching_query_param)
		{
			if ($blog_id == 0)
			{
			$sql	= "	SELECT 
							0																			AS content_blog_id, 
							dr_comments.nid 															AS context_id, 
							'C' 																		AS content_type,							
							dr_comments.cid 															AS content_id, 
							dr_comments.comment 														AS content_intro, 
							CONCAT('$sz_site_url/?q=node/',dr_comments.nid,'#comment-',dr_comments.cid)	AS content_url,
							CONCAT('$sz_site_url/?q=node/',dr_comments.nid)								AS context_url,							
							FROM_UNIXTIME(dr_comments.timestamp)										AS content_creation_date,					
							dr_comments.homepage 														AS content_author_url, 
							IF( (dr_users.mail>dr_comments.mail), dr_users.mail, dr_comments.mail)		AS content_author_email
						FROM comments dr_comments 
						LEFT JOIN sz_comment sz_content_table 
							ON dr_comments.cid=sz_content_table.comment_id 
						LEFT JOIN users dr_users 
							ON  dr_comments.uid = dr_users.uid
						INNER JOIN node dr_nodes 
							ON dr_comments.nid = dr_nodes.nid  
						WHERE dr_comments.status = '0' 
							AND dr_nodes.type!='blog' 
							AND (sz_content_table.comment_id IS NULL 
								OR (sz_content_table.posting_id=-1
									AND sz_content_table.comment_id NOT IN (SELECT comment_id FROM sz_comment WHERE posting_id!=-1)
									)
								)";				

			}
			else 
			{
			$sql	= "	SELECT 
							dr_nodes.uid																AS content_blog_id, 
							dr_comments.nid 															AS context_id, 
							'C' 																		AS content_type,
							dr_comments.cid 															AS content_id,
							dr_comments.comment 														AS content_intro,
							CONCAT('$sz_site_url/?q=node/',dr_comments.nid,'#comment-',dr_comments.cid)	AS content_url,
							CONCAT('$sz_site_url/?q=node/',dr_comments.nid)								AS context_url,
							FROM_UNIXTIME(dr_comments.timestamp)										AS content_creation_date,					
							dr_comments.homepage 														AS content_author_url, 
							IF( (dr_users.mail>dr_comments.mail), dr_users.mail, dr_comments.mail)		AS content_author_email
						FROM comments dr_comments 
						LEFT JOIN sz_comment sz_content_table 
							ON dr_comments.cid=sz_content_table.comment_id 
						LEFT JOIN users dr_users 
							ON  dr_comments.uid = dr_users.uid
						INNER JOIN node dr_nodes 
							ON dr_comments.nid = dr_nodes.nid  
						WHERE dr_nodes.uid = $blog_id
							AND dr_comments.status = '0' 
							AND dr_nodes.type='blog' 
							AND (sz_content_table.comment_id IS NULL 
								OR (sz_content_table.posting_id=-1
									AND sz_content_table.comment_id NOT IN (SELECT comment_id FROM sz_comment WHERE posting_id!=-1)
									)
								)";				
			}
		}
		return $sql;
	}
	
	
	/**
	* @ignore
	*/
	function sz_get_query_2_fetch_rc_data($comment_filter_date,$rc_size)
	{
		global $sz_db_type;
		
		switch($sz_db_type)
		{
			case 'mysql':
				$sql = "SELECT email_address, encoded_email, yk_score, global_name,
						IFNULL(
							(SELECT comments.name FROM comments WHERE comments.uid = (SELECT uid FROM users WHERE users.mail = sz_email.email_address LIMIT 1) ORDER BY TIMESTAMP DESC LIMIT 1 ),
							(SELECT comments.name FROM comments WHERE comments.mail = sz_email.email_address ORDER BY TIMESTAMP DESC LIMIT 1)
						)AS comment_author,
						NULL AS comment_author_url,  
						IFNULL(
							(SELECT cid FROM comments WHERE comments.uid = (SELECT uid FROM users WHERE users.mail = sz_email.email_address LIMIT 1) ORDER BY TIMESTAMP DESC LIMIT 1),
							(SELECT cid FROM comments WHERE comments.mail=sz_email.email_address ORDER BY TIMESTAMP DESC LIMIT 1)
						)AS comment_id, 
						IFNULL(
							(SELECT nid FROM comments WHERE comments.uid = (SELECT uid FROM users WHERE users.mail = sz_email.email_address LIMIT 1) ORDER BY TIMESTAMP DESC LIMIT 1),
							(SELECT nid FROM comments WHERE comments.mail=sz_email.email_address ORDER BY TIMESTAMP DESC LIMIT 1)
						) AS comment_post_id
						FROM sz_email WHERE EXISTS (
							SELECT * FROM sz_comment
							WHERE sz_comment.email_address = sz_email.email_address
								AND sz_comment.creation_date > $comment_filter_date
								AND sz_comment.content_type='C'
						) 
						ORDER BY yk_score DESC LIMIT $rc_size";
			break;
		}
		return $sql;
	}

	/**
	* @ignore
	*/	
	function sz_get_query_2_fetch_badge_data($email_address)
	{
		global $sz_db_type;
		
		switch($sz_db_type)
		{
			case 'mysql':				
				$sql	= "	SELECT encoded_email, yk_score, global_name, (SELECT name FROM users WHERE mail = sz_email.email_address LIMIT 1) AS comment_author FROM sz_email WHERE email_address='$email_address'";
			break;
		}
		return $sql;
		
	}
	
	/**
	* functions to query db using sqls
	*/
	
/*================================== DB RELATED FUNCTIONS =============================
======================================================================================*/
	/**
	* Executes an update/insert sql
	*
	* @param string $sql
	*/
	function sz_execute_update_insert_query($sql) {
		db_query($sql);
		return '1';
	}
	
	
	/**
	* Executes a select sql
	*
	* @param string $sql
	* @return array result set
	*/
	function sz_execute_select_query($sql) {
		$result_arr	= array();
		$result		= db_query($sql);
		while($row = db_fetch_object($result))
		{

			$result_arr[]	= get_object_vars($row);
		}
		return $result_arr;
		
	}



	/*============================= END OF ABSTRACT FUNCTIONS ==============================
	======================================================================================*/



	function sz_get_username_by_id($uid)
	{
		$sql = "SELECT name FROM users WHERE uid = '$uid'";
		return $this->sz_get_column_value_in_first_row_for_query($sql,'name');
	}



}
?>