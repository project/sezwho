<?php

/**
* @package SezWho
* @copyright (c) 2008 sezwho.com
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/


// ensure that plugin developer has set all the required global variables in sz_init.php file
$sz_init_vars	= array('sz_platform', 'sz_site_url', 'sz_plugin_url', 'sz_plugin_path', 'sz_db_type');
$sz_msg_bucket	= array();
foreach ($sz_init_vars as $var_name)
{
	if(!isset($$var_name) || $$var_name == '' || $$var_name == null)
	{
		$sz_msg_bucket[]	= '$'.$var_name;
	}
}
if(count($sz_msg_bucket) > 0)
{
	trigger_error('Attention Plugin Developer: File sz_init.php must define following global variables: '.implode(', ',$sz_msg_bucket), E_USER_ERROR);
}

// ensure that $sz_platform is of 2 characters; this is critical for proper functioning of the plugin
if(strlen($sz_platform) != 2)
{
	trigger_error("Attention Plugin Developer: File sz_init.php must define a 2 character global variable \$sz_platform to indicate the platform for which the plugin is being developed. Please <a href='mailto:support@sezwho.com?subject=Request for platform code ( my \$sz_platform=$sz_platform)'>email SezWho</a> to get the 2 character platform code.", E_USER_ERROR);
}


global $sz_js_tag_name, $sz_release, $sz_revision, $sz_server_url, $sz_plugin_version;
$sz_js_tag_name				= '2.1' ;
$sz_release					= '2.2.0';
$sz_revision				= 'r2861';
$sz_server_url 				= 'http://sezwho.com';
$sz_plugin_version			= strtoupper(substr($sz_platform, 0, 2)).substr($sz_release,0,3);


global $sz_user_image_repo, $sz_user_link_repo, $sz_user_image_repo_post, $sz_user_image_title;
$sz_user_image_repo			= 'http://s3.amazonaws.com/sz_users_images/';
$sz_user_link_repo 			= "$sz_server_url/mypublicprofile.php?commenter_email=";
$sz_user_image_repo_post 	= '_t';
$sz_user_image_title		= '';


global $sz_content_title_length, $sz_content_intro_length, $sz_context_title_length, $sz_context_intro_length, $sz_log_base, $sz_sync_block_size;
$sz_content_title_length	= 100;
$sz_content_intro_length	= 2000;
$sz_context_title_length	= 200;
$sz_context_intro_length	= 1000;
$sz_log_base				= 5;
$sz_sync_block_size 		= 100;


global $sz_platform_wrapper, $sz_handle ;
$platform				= strtolower(substr($sz_platform, 0, 2));
$platform_wrapper_name	= $platform.'_wrapper';

require_once($sz_plugin_path.'/'.$platform_wrapper_name.'.php');
require_once($sz_plugin_path.'/engine/sz_main.php');

$sz_platform_wrapper 		= new $platform_wrapper_name();
$sz_handle 					= new sz_main($sz_platform_wrapper);
?>