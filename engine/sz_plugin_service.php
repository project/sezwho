<?php

/**
* @package SezWho
* @copyright (c) 2008 sezwho.com
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

global $sz_plugin_path;
$sz_plugin_path 	= dirname(__FILE__).'/..';
require_once($sz_plugin_path.'/sz_init.php');

$method = htmlspecialchars($_GET['method']) ;


if ($method == "UpdateComment")
{
	sz_update_content();
}
else if ($method == "UpdateRating")
{
	sz_update_rating();
}

else if ($method == "UpdateEmail")
{
	sz_update_email();
}



function sz_rate_content()
{
	//DR specific code
	$referingURL=$_SERVER['HTTP_REFERER'];
	cache_clear_all($referingURL, 'cache_page',true);

	global $sz_handle, $sz_rating_count;

	$post_id 		= htmlspecialchars($_GET['postID']);
	$comment_id 	= htmlspecialchars($_GET['commentID']);
	$rater_email 	= htmlspecialchars($_GET['emailID']);
	$rating 		= htmlspecialchars($_GET['ratingIncrement']);
	$rating_type 	= 'C';
	$callback 		= htmlspecialchars($_GET['callback']);
	if ($comment_id == "0") $rating_type = 'P';

	// pass these parameters into the method that will call the postRating webservice
	$rating_response = $sz_handle->sz_functions_lib->sz_process_rating($post_id, $comment_id, $rater_email, $rating, $rating_type) ;

	if ($callback == '' || $callback == null)
	{
		echo 'SezWho.DivUtils.handleRating("'.$rating_response.'",'.$sz_rating_count.');';
	}
	else
	{
		echo $callback.'("'.$rating_response.'");';
	}
}


function sz_update_rating()
{
	global $sz_platform_wrapper;
	
	$blog_id 					= htmlspecialchars($_GET['blog_id']);
	$posting_id 				= htmlspecialchars($_GET['posting_id']);
	$comment_id 				= htmlspecialchars($_GET['comment_id']);
	$rating 					= htmlspecialchars($_GET['rating']);
	$rater_ykscore 				= htmlspecialchars($_GET['RaterYKScore']);
	$rater_email_address 		= htmlspecialchars($_GET['RaterEmail']);
	$encoded_email 				= htmlspecialchars($_GET['encoded_email']);
	$commenter_email_address 	= htmlspecialchars($_GET['CommenterEmail']);
	$commenter_ykscore 			= htmlspecialchars($_GET['CommenterYKScore']);

	/* assume that the yk score is that of the rater, then compute values and set onto the plugin db
	 see what TO DO about updating the YK score !!!*/
	if($rater_ykscore != null)
	{
		$comment = $sz_platform_wrapper->sz_execute_select_query("SELECT *
			FROM sz_comment
			WHERE comment_id='$comment_id'
					AND posting_id='$posting_id'
					AND blog_id = '$blog_id'");

		$comment			= $comment[0];
		$raw_score 			= $comment['raw_score'];
		$new_raw_score		= $rater_ykscore*($rating-5) + $raw_score;
		$new_rating_count 	= $comment['rating_count'] + 1 ;
		$comment_score 		= $sz_handle->szFunctionsLib->sz_get_content_score($new_raw_score);

		$update_comment_query = "UPDATE sz_comment
			SET comment_score='$comment_score' , raw_score='$new_raw_score' , rating_count = '$new_rating_count'
			WHERE comment_id='$comment_id'
					AND posting_id='$posting_id'
					AND blog_id = '$blog_id'";
		$sz_platform_wrapper->sz_execute_update_insert_query($update_comment_query);

		//updating Email table for commenter
		update_email_table($commenter_email_address, '', $commenter_ykscore, $encoded_email);
		//updating Email table for rater
		update_email_table($rater_email_address, '', $rater_ykscore, $encoded_email);
	}

}


function sz_update_content()
{
	global $sz_platform_wrapper, $sz_handle;
	
	//DR specific code
	cache_clear_all('*','cache_page',true);
	
	
	$blog_id 				= htmlspecialchars($_GET['blog_id']);
	$posting_id 			= htmlspecialchars($_GET['posting_id']);
	$comment_id 			= htmlspecialchars($_GET['comment_id']);
	$raw_score 				= htmlspecialchars($_GET['raw_score']);
	$temp_exclude_flag 		= htmlspecialchars($_GET['exclude_flag']);
	$exclude_flag 			= ($temp_exclude_flag == '') ? "NULL" : "'".$temp_exclude_flag."'" ;
	$commenter_yk_score 	= htmlspecialchars($_GET['commenter_yk_score']);
	$commenter_global_name 	= htmlspecialchars($_GET['Global_Name']);
	$encoded_email 			= htmlspecialchars($_GET['encoded_email']);
	$email_address 			= htmlspecialchars($_GET['email_address']);

	$comment_score = $sz_handle->sz_functions_lib->sz_get_content_score($raw_score);

	$comment_update_query 	= "	UPDATE sz_comment
								SET comment_score = '$comment_score' , 
									raw_score = '$raw_score' , 
									rating_count = 0 , 
									exclude_flag = $exclude_flag
								WHERE comment_id = $comment_id
									AND posting_id = $posting_id
									AND blog_id = $blog_id";

	$sz_platform_wrapper->sz_execute_update_insert_query($comment_update_query);
	if ($commenter_yk_score != null || $commenter_global_name != null)
	{
		//updating Email table
		update_email_table($email_address, $commenter_global_name, $commenter_yk_score, $encoded_email);
	}
}


function sz_update_email()
{
		// do the encrypt / decrypt business later
	$global_name 	= htmlspecialchars($_GET['global_name']);
	$yk_score 		= htmlspecialchars($_GET['yk_score']);
	$encoded_email 	= htmlspecialchars($_GET['encoded_email']);
	$email_address 	= htmlspecialchars($_GET['email_address']);

	//updating Email table
	update_email_table($email_address, $global_name, $yk_score, $encoded_email);
}


function update_email_table($email_address, $global_name, $yk_score, $encoded_email)
 {
	global $sz_platform_wrapper;
	$email_count_query 	= "	SELECT COUNT(*) AS no_of_email_rows
							FROM sz_email
							WHERE email_address = '$email_address'";

	$count = $sz_platform_wrapper->sz_get_column_value_in_first_row_for_query($email_count_query, 'no_of_email_rows');

	if ($count == 1)
	{ // update
		if($global_name == '')
		{
			$email_update_query	= "	UPDATE sz_email
									SET yk_score = '$yk_score'
									WHERE email_address = '$email_address'" ;
		}
		else
		{
			$email_update_query	= "	UPDATE sz_email
									SET global_name = '$global_name' , 
										yk_score = '$yk_score'
									WHERE email_address = '$email_address'" ;
		}
		$email_update_result = $sz_platform_wrapper->sz_execute_update_insert_query($email_update_query);
	}
	else
	{// if the update has failed, try an insertion
		if($global_name == '')
		{
			$email_insert_query	= "	INSERT INTO sz_email (email_address , yk_score, encoded_email)
									VALUES ('$email_address', '$yk_score', '$encoded_email')" ;
		}
		else
		{
			$email_insert_query	= "	INSERT INTO sz_email (email_address , global_name, yk_score, encoded_email)
									VALUES ('$email_address' , '$global_name' , '$yk_score', '$encoded_email')" ;
		}
		$sz_platform_wrapper->sz_execute_update_insert_query($email_insert_query);
	}
}
?>