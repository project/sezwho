<?php

/**
* @package SezWho
* @copyright (c) 2008 sezwho.com
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/


/**
* Declares few methods to be implmented in platform specific wrapper & implements few methods 
* 
* @package SezWho
* @abstract 
*/
class sz_generic_platform_wrapper
{
/**
* @ignore
*/	
	var $platform_wrapper;

	function sz_generic_platform_wrapper($sz_platform_wrapper)
	{
		$this->platform_wrapper=$sz_platform_wrapper;
	}

/*========================== BEGINING OF ABSTRACT FUNCTIONS ==========================
======================================================================================*/

	/**
	* Returns value for the key
	*
	* @param string $key
	* @return string value
	*/
	function sz_get_key_value($key)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	
	/**
	* Sets value for the key
	*
	* @param string $key
	* @param string $value
	*/
	function sz_set_key_value($key, $value)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}

	
	/**
	* Returns blog title
	*
	* @param int $blog_id
	* @return string blog title
	*/
	function sz_get_blog_title($blog_id=0)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	
	/**
	* Returns blog subject
	*
	* @param int $blog_id
	* @return string blog subject
	*/
	function sz_get_blog_subject($blog_id=0)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	
	/**
	* Returns blog url
	*
	* @param int $blog_id
	* @return string blog url
	*/
	function sz_get_blog_url($blog_id=0)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	
	/**
	* Returns the blog_id of the content
	* 
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return int id of the blog that contains the content
	*/
	function sz_get_content_blog_id(&$content)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	
	/**
	* Returns approved status of the content as in platform
	* 
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return boolean approved status
	*/
	function sz_is_content_approved(&$content)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	
	/**
	* Returns content type
	*
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return string
	*/
	 function sz_get_content_type(&$content)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	
	/**
	* Returns id of the content wrt platform
	* 
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return int id of the content wrt platform
	*/
	function sz_get_content_id(&$content)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}

	
	/**
	* Returns the creation date of the content
	* 
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return string creation date of the content wrt platform
	*/

	function sz_get_content_creation_date(&$content)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	
	/**
	* Returns subject/title of the content
	* 
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return string subject/title of the content
	*/
	function sz_get_content_title(&$content)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	
	/**
	* Returns the introduction of the content
	* 
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return string introduction of the content (preferably first 100 characters of the body of the content)
	*/
	function sz_get_content_intro(&$content)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	
	/**
	* Returns the url of the content
	* 
	* @param mixed $content either contains only contentid & contextid as $content['content_id'], $content['context_id'] or has all the fields selected in the
	* function get_query_to_fetch_unsynchroinzed_comments or similar type as the param passed to post_content
	* @return string introduction of the content (preferably first 100 characters of the body of the content)
	*/
	function sz_get_content_url(&$content)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	
	/**
	* Returns the name of the author of the content
	* 
	* @param mixed $content either contains only contentid & contextid as $content['contentid'], $content['contextid'] or similar type as the param passed to
	* post_content
	* @return string name of the author of the content
	*/
	function sz_get_content_author_name(&$content)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	
	/**
	* Returns the email of the author of the content
	* 
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return email of the author of the content
	*/
	function sz_get_content_author_email(&$content)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	
	/**
	* Returns the url of the author of the content
	* 
	* @param either contains only contentid & contextid as $content['contentid'], $content['contextid'] or similar type as the param passed to
	* post_content
	* @return url of the author of the content
	*/
	function sz_get_content_author_url(&$content)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	
	/**
	* Returns the id of the context in the platform
	* 
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return int id of the context in the platform
	*/
	function sz_get_context_id(&$content)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	
	/**
	* Returns the subject/title of the context
	* 
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return string subject/title of the context
	*/
	function sz_get_context_title(&$content)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	
	/**
	* Returns intro of the context (preferably first 100 characters of the body of the context)
	* 
	* @param either has all the fields selected in the function get_query_to_fetch_unsynchroinzed_comments or similar type as the param passed to
	* post_content
	* @return intro of the context (preferably first 100 characters of the body of the context)
	*/
	function sz_get_context_intro(&$content)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
		
	
	/**
	* Returns the url of the context
	*
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return string context url
	*/
	function sz_get_context_url(&$content)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	
	/**
	* Returns the context author name
	*
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return string context author name
	*/
	function sz_get_context_author_name(&$content)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	
	/**
	* Returns the context author email
	*
	* @param mixed $content similar data type as the param passed to sz_main::sz_post_content(&$content)
	* @return string context author email
	*/
	function sz_get_context_author_email(&$content)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	
	
	/**
	* Returns the qurey used to fetch unsynchronized platform content
	*
	* @param int $blog_id
	* @return string synchronization query
	*/
	function sz_get_unsynchroinzed_content_fetching_query($blog_id, $unsynchroinzed_content_fetching_query_param)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	
	/**
	* @ignore
	*/
	function sz_get_query_2_fetch_rc_data($comment_filter_date,$rc_size)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	/**
	* @ignore
	*/	
	function sz_get_query_2_fetch_badge_data($email_address)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	/**
	* functions to query db using sqls
	*/
	

/*================================== DB RELATED FUNCTIONS =============================
======================================================================================*/
	/**
	* Executes an update/insert sql
	*
	* @param string $sql
	*/
	function sz_execute_update_insert_query($sql)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	
	/**
	* Executes a select sql
	*
	* @param string $sql
	* @return array result set
	*/
	function sz_execute_select_query($sql)
	{
		trigger_error("Attention Plugin Developer: You have not provided platform specific implementation of the function: ".__FUNCTION__, E_USER_ERROR);
	}
	
	

/*============================= END OF ABSTRACT FUNCTIONS ==============================
======================================================================================*/

	
	/**
	* Returns blog key
	*
	* @param int $blog_id
	* @return string
	*/
	function sz_get_blog_key($blog_id=0)
	{
		$sql = "SELECT blog_key
			FROM sz_blog
			WHERE blog_id='$blog_id'";
		$blog_key=$this->platform_wrapper->sz_get_column_value_in_first_row_for_query($sql,'blog_key');
		return $blog_key;
	}


	/**
	* Returns site key
	*
	* @return string
	*/
	function sz_get_site_key()
	{
		$sql = "SELECT site_key FROM sz_site ";
		$site_key=$this->platform_wrapper->sz_get_column_value_in_first_row_for_query($sql,"site_key");
		return $site_key;
	}

	
	/**
	* Returns value of a column in first row for a sql
	*
	* @param string $sql
	* @param string $column_name
	* @return mixed
	*/
	function sz_get_column_value_in_first_row_for_query($sql,$column_name)
	{
		$result_arr=$this->sz_execute_select_query($sql);
		$column_value=$result_arr[0][$column_name];
		return $column_value;
	}
}
?>