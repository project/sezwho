<?php

/**
* @package SezWho
* @copyright (c) 2008 sezwho.com
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/


/**
* Contains the implementation of few platform-SezWho interface APIs. Also contains few library functions.
* 
* @package SezWho
*/
class sz_functions_lib
{
	/**
	* @ignore
	*/
	var $platform_wrapper;

	function sz_functions_lib($platform_wrapper)
	{
		$this->platform_wrapper=$platform_wrapper;
	}


	/**
	* Does the setup of the SezWho cache table in platform DB
	*
	* @return string '1' on success else errorcode
	*/
	function sz_setup_db()
	{
		global $sz_db_type;
		$queries = array();
		$queries = $this->sz_fetch_cache_table_queries();
		foreach($queries as $query_type => $query)
		{
			if(!($this->platform_wrapper->sz_execute_update_insert_query($query)))
			{
				$return_val = 'SZ_ERR_'.$sz_db_type.'_'.$query_type.'_QUERY';
				break;
			}
			else
			{
				$return_val = '1';
			}
		}
		return $return_val;
	}
	

	

	
	/**
	* Returns the script that would fetch JS & CSS files and populate global configuration parameters.
	*
	* @return string header script
	*/
	function sz_get_header_script($blog_id)
	{
		$site_key	= $this->platform_wrapper->sz_get_site_key();
		$blog_key	= $this->platform_wrapper->sz_get_blog_key($blog_id);

		$css_file_fetching_script	= $this->sz_get_css_file_fetching_script($site_key,$blog_key);
		$js_file_fetching_script	= $this->sz_get_js_file_fetching_script($site_key,$blog_key);
		$global_params_script		= $this->sz_get_global_params_script($site_key,$blog_key,$blog_id);

		$header_script = $css_file_fetching_script.$js_file_fetching_script.$global_params_script;

		return $header_script;
	}

	
	/**
	* Returns script for fetching CSS file
	*
	* @param string $site_key
	* @param string $blog_key
	* @return string
	*/
	function sz_get_css_file_fetching_script($site_key,$blog_key)
	{
		global $sz_server_url, $sz_plugin_version, $sz_js_tag_name ;

		$theme_name	= 'default';
		$platform 	= strtolower(substr($sz_plugin_version, 0, 2));
		$version	= substr($sz_plugin_version, 2) ;

		$css_script_src = "$sz_server_url/widgets/profile/css_output/$platform/$theme_name/$version/$sz_js_tag_name/$site_key/$blog_key.css";
		$css_file_fetching_script="<link rel='stylesheet' id='szProfAndEmbedStyleSheet' href='$css_script_src' type='text/css' media='screen' />\n";

		return $css_file_fetching_script;

	}


	/**
	* Returns script for fetching JS file
	*
	* @param string $site_key
	* @param string $blog_key
	* @return string
	*/
	function sz_get_js_file_fetching_script($site_key, $blog_key)
	{
		global $sz_server_url, $sz_plugin_version, $sz_js_tag_name;

		$theme_name = 'default';
		$platform 	= substr($sz_plugin_version, 0, 2) ;
		$version	= substr($sz_plugin_version, 2) ;

		//$script_src = $sz_server_url ."/widgets/profile/js_generator.php?site_key=$site_key&theme=$theme_name&plugin_version=$version&platform=$platform&js_tag_name=$sz_js_tag_name&blog_key=$blog_key" ;
		$js_script_src 				= "$sz_server_url/widgets/profile/js_output/$platform/$theme_name/$version/$sz_js_tag_name/$site_key/$blog_key";
		$js_file_fetching_script 	= "<script type='text/javascript' src='$js_script_src'></script>";

		return $js_file_fetching_script;
	}


	/**
	* Returns SezWho global parameters script
	*
	* @param string $site_key
	* @param string $blog_key
	* @param int $blog_id
	* @return string
	*/
	function sz_get_global_params_script($site_key, $blog_key, $blog_id)
	{
		global $sz_server_url, $sz_plugin_url, $sz_plugin_version, $sz_site_url;

		$version = substr($sz_plugin_version, 2) ;

		$global_params_script= "<script type=\"text/javascript\">";

		// drupal specific code
		$global_params_script .= 'var sz_global_config_params = {cppluginurl:"'.$sz_plugin_url.'",cpserverurl:"'.$sz_server_url.'", sitekey:"'.$site_key.'",blogkey:"'.$blog_key.'",blogid:"'.$blog_id.'",rating_submit_path:"'.$sz_site_url.'/?q=sezwho/services/rate_content/",plugin_version:"'.$version.'"} ; ';
		$global_params_script .= "</script>";

		return $global_params_script;
	}

	
	/**
	 * 
	 */
	function sz_is_content_posted_2_sz(&$content)
	{
		$is_content_posted_2_sz	= (isset($content['is_content_posted_2_sz'])) ? $content['is_content_posted_2_sz'] : '';

		if ($is_content_posted_2_sz=='' || $is_content_posted_2_sz==null)
		{
			$blog_id 		= $this->platform_wrapper->sz_get_content_blog_id($content);
			$context_id		= $this->platform_wrapper->sz_get_context_id($content);
			$content_id		= $this->platform_wrapper->sz_get_content_id($content);
			$content_type	= strtoupper($this->platform_wrapper->sz_get_content_type($content));

			
			$sql	= "SELECT COUNT(*) AS no_of_existing_contents FROM sz_comment WHERE blog_id=$blog_id AND posting_id='$context_id' AND comment_id=$content_id AND content_type='$content_type' AND exclude_flag IS NULL";
			$no_of_existing_contents	= $this->platform_wrapper->sz_get_column_value_in_first_row_for_query($sql,'no_of_existing_contents');
			if($no_of_existing_contents>0)
			{
				$content['is_content_posted_2_sz']	= true;
			}
			else
			{
				$content['is_content_posted_2_sz']	= false;
			}
		}

		return $content['is_content_posted_2_sz'];
	}


	/**
	* Returns content configuration-parameter initialization script
	*
	* @param int $context_id
	* @return string
	*/
	function sz_get_content_config_params_init_script(&$content)
	{
		global $sz_auto_comment, $sz_auto_option_bar, $sz_enable_content_rating, $sz_enable_content_filter, $sz_user_link_repo, $sz_user_image_repo, $sz_user_image_repo_post, $sz_user_link_repo_use_enc, $sz_show_commenter_pic, $sz_user_image_title;

		$blog_id	= $this->platform_wrapper->sz_get_content_blog_id($content);
		$context_id	= $this->platform_wrapper->sz_get_context_id($content);

		$sql = "	SELECT COUNT(posting_id) AS no_of_contents
					FROM sz_comment
					WHERE	blog_id		= $context_id
						AND posting_id	= $context_id";

		$no_of_contents = $this->platform_wrapper->sz_get_column_value_in_first_row_for_query($sql,'no_of_contents');
		$sort_order 	= $_GET['sortOrder'] ;

		$content_config_params_init_script="<script type='text/javascript'> ";
		$content_config_params_init_script .='var sz_comment_config_params = {post_id:"'.$context_id.'",sortOrder:"'.$sort_order.'",sz_auto_comment:'.$sz_auto_comment.',sz_auto_option_bar:'.$sz_auto_option_bar.',sz_enable_comment_rating:'.$sz_enable_content_rating.',sz_enable_comment_filter:'.$sz_enable_content_filter.',comment_number:'.$no_of_contents.',sz_user_link_repo:"'.$sz_user_link_repo.'",sz_user_image_repo:"'.$sz_user_image_repo.'",sz_user_image_repo_post:"'.$sz_user_image_repo_post.'", sz_user_link_repo_use_enc:'.$sz_user_link_repo_use_enc.',sz_show_commenter_pic:'.$sz_show_commenter_pic.',sz_commenter_pic_title:"'.$sz_user_image_title.'", sz_comment_data:[]}';
		$content_config_params_init_script .="</script>";

		return $content_config_params_init_script;
	}

	
	/**
	* Returns script containing content configuration parameter information required by image, profile & rating/score bar script
	*
	* @param int $content_id
	* @return string content configuration-parameter script
	*/
	function sz_get_content_config_params_script(&$content)
	{
		global $sz_counter;

		if( !$this->sz_is_content_posted_2_sz($content) )
		{
			return ''; // the content is not posted to SezWho
		}
		$this->sz_dig_content_related_data($content);		

		$content_config_params_script		= '';
		
		$context_id	= $content['context_id'];
		if('-1'==$context_id)
		{
			$content_config_params_script 	.= "<script type='text/javascript'> if(!sz_post_config_params){var sz_post_config_params = new Array();}";
			$content_config_params_script	.= "sz_post_config_params[{$content['content_id']}] = {post_id:'{$content['content_id']}', blog_author_name:'".rawurlencode($content['content_author_name'])."', blog_author_email:'{$content['content_author_encrypted_email']}',md5email:'".md5(strtolower($content['content_author_email']))."',sz_score:'{$content['content_author_yk_score']}', post_score:'{$content['content_score']}'};";
			$content_config_params_script	.=	' </script>';

			return $content_config_params_script;
		}
		else
		{
			if($sz_counter==0)
			{
				$content_config_params_script 	.= $this->sz_get_content_config_params_init_script($content);
			}

			$content_config_params_script		.= "<script type='text/javascript' id='szCommentHiddenTag:".$content['content_id']."'> ";
			$content_config_params_script 		.= "sz_comment_config_params.sz_comment_data[$sz_counter]= {comment_id:'{$content['content_id']}', comment_author:'".rawurlencode($content['content_author_name'])."', comment_author_url:'{$content['content_author_url']}', comment_author_email:'{$content['content_author_encrypted_email']}', encoded_email:'".md5(strtolower($content['content_author_email']))."', sz_score:'{$content['content_author_yk_score']}',comment_score:'{$content['content_score']}'};";
			$content_config_params_script		.= ' </script>';

			return $content_config_params_script;
		}
	}
	
	

	/**
	* Returns script for creating content author image
	*
	* @param int $content_id
	* @return string author image script
	*/	
	function sz_get_content_author_image_script(&$content)
	{
		// following global data is dug in the method: sz_dig_content_related_data()
		global $sz_counter, $sz_enable_auto_layout, $sz_user_image_repo_post;

		if(!$sz_enable_auto_layout)
		{
			return '';
		}

		if( !$this->sz_is_content_posted_2_sz($content) )
		{
			return ''; // the content is not posted to SezWho
		}
		$this->sz_dig_content_related_data($content);		

		$context_id	= $content['context_id'];
		if('-1'==$context_id)
		{
			$content_author_image_script	= "<img src='http://s3.amazonaws.com/sz_users_images/noimg.gif' ownmouseout='javascript:SezWho.DivUtils.cancelPopUp();' onmousedown='javascript:void SezWho.Utils.cpProfileImgClickHandler({$content['content_id']}, \"P\");' onmouseover='javascript:SezWho.Utils.cpProfilePostEventHandler(event)' class='cpEmbedImageAuthor' onerror=\"this.src='http://s3.amazonaws.com/sz_users_images/noimg.gif'\" id='sz_image_link_post:{$content['content_id']}' alt='User Image' title='No image'/><script type='text/javascript' src='http://image.sezwho.com/getpic.php?md5id=".md5(strtolower($content['content_author_email']))."&amp;return_js=true&amp;aname=".rawurlencode($content['content_author_name'])."&amp;element_id=sz_image_link_post:{$content['content_id']}'></script>";
		}
		else
		{
			$content_author_image_script	= "<img src='http://s3.amazonaws.com/sz_users_images/".md5(strtolower($content['content_author_email'])).$sz_user_image_repo_post."' alt='no image' title='no image' class='cpEmbedImageUserh' onmouseover='javascript:SezWho.Utils.cpProfileEventHandler(event);' onmousedown='javascript:SezWho.Utils.cpProfileEventHandler(event);' href='' onmouseout='javascript:SezWho.DivUtils.cancelPopUp();' onerror=\"this.src='http://s3.amazonaws.com/sz_users_images/noimg.gif'\" id='sz_image_link:$sz_counter'/><script type='text/javascript' src='http://image.sezwho.com/getpic.php?md5id=".md5(strtolower($content['content_author_email']))."&return_js=true&element_id=sz_image_link:$sz_counter&aname=".rawurlencode($contnet['content_author_name'])."'></script>";
		}


		return $content_author_image_script;
	}


	/**
	* Returs script for creating profile link
	*
	* @param int $content_id
	* @return string profile link script
	*/	
	function sz_get_content_author_profile_link_script(&$content)
	{
		global $sz_counter, $sz_enable_auto_layout;

		if(!$sz_enable_auto_layout)
		{
			return '';
		}

		if( !$this->sz_is_content_posted_2_sz($content) )
		{
			return ''; // the content is not posted to SezWho
		}
		$this->sz_dig_content_related_data($content);


		$context_id	= $content['context_id'];
		if('-1'==$context_id)
		{
			$content_author_profile_link_script = "<span id='sz_author_span_post:{$content['content_id']}'> (<a href='#' onmouseout='javascript:SezWho.DivUtils.cancelPopUp();' onmousedown='javascript:void SezWho.Utils.cpProfilePostEventHandler(event);' onmouseover='javascript:SezWho.Utils.cpProfilePostEventHandler(event)' class='cpEmbedPageProfileLink' id='sz_author_link_post:{$content['content_id']}'/>Check me out!</a>)</span>";

		}
		else
		{
			$content_author_profile_link_script = " (<a class='cpEmbedPageProfileLinkCustom' id='sz_profile_link:$sz_counter' onmouseover='javascript:SezWho.Utils.cpProfileEventHandler(event);' onmousedown='javascript:SezWho.Utils.cpProfileEventHandler(event);' href='' onmouseout='javascript:SezWho.DivUtils.cancelPopUp();'>Check me out!</a>)";
		}
		return $content_author_profile_link_script;
	}


	/**
	* Returns script for creating rating/score bar
	*
	* @param int $content_id
	* @return string rating/score bar script
	*/	
	function sz_get_content_rating_n_score_bar_script(&$content)
	{
		global $sz_counter, $sz_enable_auto_layout, $sz_enable_content_rating;

		if(!$sz_enable_auto_layout)
		{
			return '';
		}

		if(!$sz_enable_content_rating)
		{
			return '';
		}

		if( !$this->sz_is_content_posted_2_sz($content) )
		{
			return ''; // the content is not posted to SezWho
		}
		$this->sz_dig_content_related_data($content);

		$width 			= ((float)$content['content_score']);
		$width 			= (int)($width*10);
		$print_score 	= sprintf("%1.1f", $content['content_score']/2);

		$context_id	= $content['context_id'];
		if('-1'==$context_id)
		{

			$content_footer_script	=  "<table class='cpEmbedPageTable' style='width:auto'>";
			$content_footer_script	.=  "<tr>";
			$content_footer_script	.=  "<td class='cpEmbedPageTableCell'>";
			$content_footer_script	.= "<span class='cpEmbedPagePostFooterCS'><a href='#' onmousedown='SezWho.DivUtils.activateRatingsHelpDIV(event);'>Rate this</a>: </span>";
			$content_footer_script	.=  "</td><td class='cpEmbedPageTableCell'>";
			$content_footer_script	.= '<ul id="cpEmbedPostScore:'.$content['content_id'].'" class="cpEmbedPostScoreUl" onmousedown="SezWho.Utils.ScoreDisplay.update(event)" onmousemove="SezWho.Utils.ScoreDisplay.cur(event)" title="Rate This"><li id="cpEmbedPostScoreLi:'.$content['content_id'].'" class="cpEmbedPostScoreLi" title="'.$print_score.'" style="width:'.$width.'%"></li></ul>';
			$content_footer_script	.=  "</td><td class='cpEmbedPageTableCell'>";
			$content_footer_script	.= '<span id="cpEmbedPostScoreSpan:'.$content['content_id'].'" class="cpEmbedPostScoreSpan" title="'.$print_score.'">'.$print_score.'</span>';
			if ($content['sz_total_rating_count'])
			$content_footer_script	.= '<span id="cpEmbedPostCountSpan:'.$content['content_id'].'" class="cpEmbedPostCountSpan" title="'.$content['sz_total_rating_count'].'"> ( '.$content['sz_total_rating_count'].' ratings ) </span>';
			else
			$content_footer_script	.= '<span id="cpEmbedPostCountSpan:'.$content['content_id'].'" class="cpEmbedPostCountSpan" title="'.$content['sz_total_rating_count'].'">( not yet rated )</span>';
			$content_footer_script	.=  "</td>";
			$content_footer_script	.= '</tr>';
			$content_footer_script	.= '</table>';
		}
		else
		{

			$content_footer_script 		=  "<table class='cpEmbedPageTable' style='width:auto'>";
			$content_footer_script		.=  "<tr>";
			$content_footer_script 		.=  "<td class='cpEmbedPageTableCell'>";
			$content_footer_script 		.= "<span class='cpEmbedPageCommFooterCS'><a href='#' onmousedown='SezWho.DivUtils.activateRatingsHelpDIV(event);'>Rate this</a>: </span>";
			$content_footer_script 		.=  "</td><td class='cpEmbedPageTableCell'>";
			$content_footer_script 		.= '<ul id="cpEmbedCommScore:'.$sz_counter.'" class="cpEmbedCommScoreUl" onmousedown="SezWho.Utils.ScoreDisplay.update(event)" onmousemove="SezWho.Utils.ScoreDisplay.cur(event)" title="Rate This!"><li id="cpEmbedCommScoreLi:'.$sz_counter.'" class="cpEmbedCommScoreLi" title="'.$print_score.'" style="width:'.$width.'%"></li></ul>';
			$content_footer_script 		.=  "</td><td class='cpEmbedPageTableCell'>";
			$content_footer_script 		.= '<span id="cpEmbedCommScoreSpan:'.$sz_counter.'" class="cpEmbedCommScoreSpan" title="'.$print_score.'">'.$print_score.'</span>';
			if ($content['sz_total_rating_count'])
			{
				$content_footer_script	.= '<span id="cpEmbedCommCountSpan:'.$sz_counter.'" class="cpEmbedCommCountSpan" title="'.$content['sz_total_rating_count'].'"> ( '.$content['sz_total_rating_count'].' ratings )</span>';
			}
			else
			{
				$content_footer_script 	.= '<span id="cpEmbedCommCountSpan:'.$sz_counter.'" class="cpEmbedCommCountSpan" title="'.$content['sz_total_rating_count'].'">( not yet rated )</span>';
			}
			$content_footer_script 		.=  "</td>";
			$content_footer_script 		.= '</tr>';
			$content_footer_script 		.= '</table>';
		}
		return $content_footer_script;
	}


	/**
	* Finds the content related data and assigns those to global variables that are used in other get_xxx_script methods.
	*
	* @param int $content_id
	*/
	function sz_dig_content_related_data(&$content)
	{
		global $sz_blog_id_of_last_dug_content, $sz_context_id_of_last_dug_content, $sz_content_id_of_last_dug_content, $sz_content_type_of_last_dug_content;
		global $sz_counter;

		$blog_id		= $this->platform_wrapper->sz_get_content_blog_id($content);
		$context_id		= $this->platform_wrapper->sz_get_context_id($content);
		$content_id		= $this->platform_wrapper->sz_get_content_id($content);
		$content_type	= $this->platform_wrapper->sz_get_content_type($content);

		if(	$blog_id==$sz_blog_id_of_last_dug_content &&
			$context_id==$sz_context_id_of_last_dug_content &&
			$content_id==$sz_content_id_of_last_dug_content &&
			$content_type==$sz_content_type_of_last_dug_content )
		{
			return;
		}

		$sql = "	SELECT 	sz_email.email_address 			AS content_author_email, 
							sz_email.encoded_email 			AS content_author_encrypted_email, 
							sz_email.yk_score 				AS content_author_yk_score, 
							sz_comment.comment_score 		AS content_score,  
							sz_comment.anon_raw_score 		AS content_anonymous_raw_score, 
							sz_comment.rating_count 		AS content_rating_count, 
							sz_comment.anon_rating_count 	AS content_anonymous_rating_count
					FROM sz_comment, sz_email
					WHERE 	sz_comment.blog_id			= $blog_id
						AND sz_comment.posting_id		= $context_id
						AND	sz_comment.comment_id		= $content_id
						AND sz_comment.content_type		= '$content_type'
						AND sz_comment.email_address	= sz_email.email_address
						AND sz_comment.exclude_flag 	IS NULL";

		$result 				= $this->platform_wrapper->sz_execute_select_query($sql);

		$content['context_id']						= $context_id;
		$content['content_author_email']			= $result[0]['content_author_email'];
		$content['content_author_encrypted_email']	= $result[0]['content_author_encrypted_email'];
		$content['content_author_name']				= $this->platform_wrapper->sz_get_content_author_name($content);
		$content['content_author_url']				= $this->platform_wrapper->sz_get_content_author_url($content);
		$content['content_author_yk_score']			= number_format($result[0]['content_author_yk_score'],1);
		$content['content_score']					= $result[0]['content_score'];

		$content['content_anonymous_raw_score']		= empty($result[0]['content_anonymous_raw_score'])?0:$result[0]['content_anonymous_raw_score'];
		$content['content_score']					= number_format($this->sz_apply_anonymous_score($content['content_anonymous_raw_score'], $content['content_score']), 1);
		$content['content_rating_count']			= $result[0]['content_rating_count'];
		$content['content_anonymous_rating_count']	= $result[0]['content_anonymous_rating_count'];
		$content['sz_total_rating_count']			= (empty($content['content_rating_count'])?0:$content['content_rating_count']) + (empty($content['content_anonymous_rating_count'])?0:$content['content_anonymous_rating_count']);

		if('-1'!=$context_id)
		{
			if(!isset($sz_counter))
			{
				$sz_counter=0;
			}
			else
			{
				$sz_counter++;
			}
		}

		$sz_blog_id_of_last_dug_content			= $blog_id;
		$sz_context_id_of_last_dug_content		= $context_id;
		$sz_content_id_of_last_dug_content		= $content_id;
		$sz_content_type_of_last_dug_content	= $content_type;

		return '1';
	}


	/**
	* Algorithm for applying anonymous score to the regular score
	*
	* @param float $anon_raw_score
	* @param float $regular_score
	* @return float
	*/
	function sz_apply_anonymous_score($anon_raw_score, $regular_score)
	{
		$n = $anon_raw_score / 25;

		$regular_score += ( 1 - pow(0.5, $n) );

		if($regular_score > 10)
		{
			$regular_score = 10;
		}
		elseif($regular_score < 0)
		{
			$regular_score = 0;
		}

		return $regular_score;
	}

	
	/**
	* Calculates comment score based on raw score
	*
	* @param float $raw_score
	* @return float
	*/
	function sz_get_content_score($raw_score)
	{
		global $sz_log_base;
		
		$comment_score = 5;

		if($raw_score >1)
		{
			$comment_score =log($raw_score,$sz_log_base)+5;
		}
		else if($raw_score < -1)
		{
			$comment_score = (-1*log(-1*$raw_score,$sz_log_base))+5;
		}
		return $comment_score;
	}

	
	/**
	 * Formats a string by:
	 * 1) replacing the characters (default: Linefeed \n and Carriage Return \r with space ' ')
	 * 2) Removing html tags
	 * 3) Reducing the lenght of the string
	 * 
	 * @param string 
	 * @param int
	 * @param array
	 * @param char
	 * @return string
	 */
	
	function sz_format_string($string_2_format, $length=0, $chars_to_be_replaced='', $replace_with='' )
	{
		$formatted_string='';
		if($length==0)
		{
			$length					= strlen($string_2_format);
		}
		
		if($chars_to_be_replaced=='')
		{
			$chars_to_be_replaced	= array("\n","\r");
		}
		
		if($replace_with=='')
		{
			$replace_with			= ' ';
		}
		
		
		$formatted_string	= str_replace($chars_to_be_replaced, $replace_with, $string_2_format);
		$formatted_string	= strip_tags($formatted_string);
		$formatted_string	= substr($formatted_string, 0, $length);
		
		return $formatted_string;
	}

	
	/**
	* Parses a string to return key-value pair
	*
	* @param string $string_dictionary having format <key1=value1,key2=value2,key3=value3>
	* @param string $pair_separator
	* @param string $key_value_separator
	* @return array
	*/
	function sz_get_key_value_pairs_from_string_dictionary($string_dictionary, $pair_separator=",",$key_value_separator="=")
	{
		$result_arr = array();
		$returned_values = explode($pair_separator, $string_dictionary);
		foreach($returned_values as $item)
		{
			list($key, $value) = explode($key_value_separator, $item, 2);
			$result_arr[$key] = $value;
		}
		return $result_arr;
	}


	/**
	* Sends http request to the SezWho server (mostly used for webservice calls)
	*
	* @param string $request
	* @param string $host SezWho server url
	* @param string $path webservice url
	* @param int $port
	* @return string response from the SezWho server for the webservice
	*/
	function sz_http_post_2_server($request, $host, $path, $port = 80)
	{

		global $sz_plugin_version, $sz_revision;

		$cpserver= substr($host, 7);

		$cpserver_params = split( "/" , $cpserver);
		if (strpos($cpserver , ':') > 0 )
		{
			$cpserver_port_arr = split (':' , $cpserver_params[0]) ;
			$cpserver = $cpserver_port_arr[0] ;
		} else
		{
			$cpserver = $cpserver_params[0] ;
		}
		$cpserver_path = $cpserver_params[1].$path;
		$http_request  = "POST /$cpserver_path HTTP/1.0\r\n";
		$http_request .= "Host: $cpserver\r\n";
		$http_request .= "Content-Type: application/x-www-form-urlencoded; charset=UTF-8\r\n";
		$http_request .= "Content-Length: " . strlen($request) . "\r\n";
		$http_request .= "User-Agent: SezWho/$sz_plugin_version.$sz_revision\r\n";
		$http_request .= "\r\n";
		$http_request .= $request;

		$response = '';
		if( false != ( $fs = @fsockopen($cpserver, $port, $errno, $errstr, 10) ) )
		{
			fwrite($fs, $http_request);
			while ( !feof($fs) )
			{
				$response .= fgets($fs, 1160); // One TCP-IP packet
			}
			fclose($fs);
			$response = explode("\r\n\r\n", $response, 2);
			return $response[1];
		}
		return $response;
	}

	

	/**
	* Synchronizes the platform content data with SezWho
	*
	* @param int $blog_id
	* @return int|string Number of contents synchronized on success, blank string if no action taken else errorcode
	*/

	function sz_synchronize_content($blog_id, $unsynchroinzed_content_fetching_query_param)
	{
		global $sz_http_post_data, $p_db_data, $sz_sync_block_size;

		$sql	= trim($this->platform_wrapper->sz_get_unsynchroinzed_content_fetching_query($blog_id, $unsynchroinzed_content_fetching_query_param));
		
		if( $sql=='' || $sql==null )
		{
			return;
		}
		$sql_result 				= $this->platform_wrapper->sz_execute_select_query($sql);
		$number_of_contents_2_sync 	= count($sql_result);
		if ($number_of_contents_2_sync==0)
		{
			return 0;
		}

		$site_key	= $this->platform_wrapper->sz_get_site_key();
		if($site_key=='' || $site_key==null)
		{
			return 'SZ_ERR_NO_SITE_KEY';
		}
		$blog_key	= $this->platform_wrapper->sz_get_blog_key($blog_id);
		if($blog_key=='' || $blog_key==null)
		{
			return 'SZ_ERR_NO_BLOG_KEY';
		}		

		if(!isset($p_db_data))
		{
			$p_db_data	= $this->sz_get_existing_context_ids();
		}

		//$e_db_data	= $this->sz_get_existing_email_addresses_from_sz_email_table();
		if (!isset($e_db_data))
		{
			$e_db_data	= array();
		}

		$tosync 	= $number_of_contents_2_sync;
		$from 		= 0 ;

		while ($tosync > 0)
		{
			$s_chunk 			= ($tosync > $sz_sync_block_size)? $sz_sync_block_size : $tosync;
			$posting_num 		= 0;
			$comment_num		= 0;
			$email_num 			= 0;
			$sz_http_post_data 	= array();

			for ($i=0 ; $i<$s_chunk ; $i++)
			{
				$content = $sql_result[$i+$from];

				if(!$this->platform_wrapper->sz_is_content_approved($content))
				{
					continue;
				}

				$content_author_email	= $this->platform_wrapper->sz_get_content_author_email($content);
				if($content_author_email=='' || $content_author_email==null)
				{
					continue;
				}

				$content_blog_id		= $this->platform_wrapper->sz_get_content_blog_id($content);
				$context_id				= $this->platform_wrapper->sz_get_context_id($content);
				$content_id				= $this->platform_wrapper->sz_get_content_id($content);
				$content_type			= $this->platform_wrapper->sz_get_content_type($content);
				$content_creation_date	= $this->platform_wrapper->sz_get_content_creation_date($content);

				$sql= "INSERT INTO sz_comment (blog_id, posting_id, comment_id, content_type, creation_date, comment_rating, comment_score, raw_score, rating_count, email_address, exclude_flag)
					VALUES ($content_blog_id, $context_id, $content_id, '$content_type', '$content_creation_date' , NULL, NULL, NULL, NULL , '$content_author_email','S')";
				$this->platform_wrapper->sz_execute_update_insert_query($sql);

				// add the comment data to the http array
				
				if('-1'==$context_id)
				{//this does not have a context, hence considered as a post from server perspective
					if( !array_key_exists($content_id, $p_db_data) )
					{
						$this->sz_dump_content($posting_num, $content);
						$p_db_data[$content_id] = 1;
						$posting_num++;
					}
				}
				else
				{//this has a context and hence considered as comment from server perspective
					$this->sz_dump_content($i, $content);

					if (!array_key_exists($context_id, $p_db_data))
					{
						$this->sz_dump_context($posting_num, $content);
						$p_db_data[$context_id] = 1;
						$posting_num++;
					}
					
					$comment_num++;
				}


				if (!array_key_exists ($content_author_email, $e_db_data))
				{
					$sz_http_post_data['EMAIL-'.$email_num] = urlencode($content_author_email);
					$e_db_data[$content_author_email] = 1;
					$email_num++;
				}
			}
			$sz_http_post_data["POSTCOUNT"] 	= $posting_num;
			$sz_http_post_data["COMMENTCOUNT"] 	= $comment_num;
			$sz_http_post_data["EMAILCOUNT"] 	= $email_num;
			$sz_http_post_data["sitekey"]		= $site_key;
			$sz_http_post_data["blogkey"]		= $blog_key;
			$sz_http_post_data["blogid"]		= $blog_id;

			$response = $this->sz_synchronize_data_with_server();
			$attempt=$this->sz_process_response($response);
			if($attempt !='1')
			{
				return $attempt;
			}
			$tosync	= $tosync - $sz_sync_block_size;
			$from 	= $from + $sz_sync_block_size;
		}
		return $number_of_contents_2_sync;

	}

	
	/**
	* Finds distinct posting ids for all the contents available in comment table
	*
	* @param int $blog_id
	* @return array index: posting id, value=1 (for all indexes)
	*/
	function sz_get_existing_context_ids()
	{
		$posting_ids = array();
		
		$sql = "SELECT DISTINCT posting_id FROM sz_comment WHERE posting_id!=-1";
		$result_array = $this->platform_wrapper->sz_execute_select_query($sql);
		for($i=0;$i<count($result_array);$i++)
		{
			$posting_ids[$result_array[$i]['posting_id']] = 1;
		}		
		return $posting_ids;
	}


	/**
	* Finds distinct emails for all the contents available in comment table
	*
	* @param int $blog_id
	* @return array index: email, value=1 (for all indexes)
	*/	
	function sz_get_existing_email_addresses_from_sz_email_table()
	{
		$email_addresses 		= array();
		
		$sql = "SELECT email_address FROM sz_email";
		$result_array 	= $this->platform_wrapper->sz_execute_select_query($sql);
		for($i=0;$i<count($result_array);$i++)
		{
			$email_addresses[$result_array[$i]['email_address']] = 1;
		}

		return $email_addresses;
	}


	/**
	* Dumps comment type content data that need to be sent to the SezWho server for synchronization
	*
	* @param int $index
	* @param mixed $content
	*/
	function sz_dump_content($index, $content)
	{
		global $sz_http_post_data;
		global $sz_context_title_length, $sz_context_intro_length, $sz_content_intro_length;
		
		$context_id	= $this->platform_wrapper->sz_get_context_id($content);

		if('-1'==$context_id)
		{
			$sz_http_post_data["POSTID-".$index] 	= $this->platform_wrapper->sz_get_content_id($content);
			$sz_http_post_data["POSTDATE-".$index] 	= $this->platform_wrapper->sz_get_content_creation_date($content);
			$sz_http_post_data["POSTTITLE-".$index] = urlencode($this->sz_format_string($this->platform_wrapper->sz_get_content_title($content), $sz_context_title_length));
			$sz_http_post_data["POSTINTRO-".$index] = urlencode($this->sz_format_string($this->platform_wrapper->sz_get_content_intro($content), $sz_context_intro_length));
			$sz_http_post_data["POSTURL-".$index] 	= urlencode(($this->platform_wrapper->sz_get_content_url($content)));
			$sz_http_post_data["POSTEMAIL-".$index] = urlencode($this->platform_wrapper->sz_get_content_author_email($content));
			$sz_http_post_data["POSTTAGS-".$index] 	= '';
		}
		else
		{
			$sz_http_post_data["COMMENTPOSTID-".$index] 		= $this->platform_wrapper->sz_get_context_id($content);
			$sz_http_post_data["COMMENT-".$index] 				= $this->platform_wrapper->sz_get_content_id($content);
			$sz_http_post_data["COMMENTDATE-".$index] 			= $this->platform_wrapper->sz_get_content_creation_date($content);
			$sz_http_post_data["COMMNETINTRO-".$index]			= urlencode($this->sz_format_string($this->platform_wrapper->sz_get_content_intro($content), $sz_content_intro_length));
			$sz_http_post_data["COMMENTURL-".$index] 			= urlencode($this->platform_wrapper->sz_get_content_url($content));			
			$sz_http_post_data["COMMENTAUTHORURL-".$index] 		= urlencode($this->platform_wrapper->sz_get_content_author_url($content));
			$sz_http_post_data["COMMENTAUTHOREMAIL-".$index]	= urlencode($this->platform_wrapper->sz_get_content_author_email($content));
		}
	}

	/**
	* Dumps post type content data that need to be sent to the SezWho server for synchronization
	*
	* @param int $index
	* @param mixed $content
	*/
	function sz_dump_context($index, $content)
	{
		global $sz_http_post_data;
		global $sz_context_title_length, $sz_context_intro_length;

			$sz_http_post_data["POSTID-".$index] 	= $this->platform_wrapper->sz_get_context_id($content);
			$sz_http_post_data["POSTDATE-".$index] 	= $this->platform_wrapper->sz_get_content_creation_date($content);
			$sz_http_post_data["POSTTITLE-".$index] = urlencode($this->sz_format_string($this->platform_wrapper->sz_get_context_title($content), $sz_context_title_length));
			$sz_http_post_data["POSTINTRO-".$index] = urlencode($this->sz_format_string($this->platform_wrapper->sz_get_context_intro($content), $sz_context_intro_length));
			$sz_http_post_data["POSTURL-".$index] 	= urlencode(($this->platform_wrapper->sz_get_context_url($content)));
			$sz_http_post_data["POSTEMAIL-".$index] 	= urlencode($this->platform_wrapper->sz_get_context_author_email($content));
			$sz_http_post_data["POSTTAGS-".$index] 	= '';
	}


	/**
	* Sends synchronization data to the SezWho server
	*
	* @param string $cpserver
	* @return string response from the SezWho server
	*/
	function sz_synchronize_data_with_server()
	{
		global $sz_http_post_data;
		global $sz_server_url, $sz_plugin_version, $sz_revision;

		$sz_server			= substr($sz_server_url, 7);
		$sz_server_params 	= split( "/" , $sz_server);
		if (strpos($sz_server , ':') > 0 )
		{
			$sz_server_port_arr = split (':' , $sz_server_params[0]) ;
			$sz_server = $sz_server_port_arr[0] ;
		}
		else
		{
			$sz_server = $sz_server_params[0] ;
		}
		$sz_server_path = $sz_server_params[1].'/webservices/yksyncblogservice.php';

		$keys = array_keys($sz_http_post_data) ;
		$key ='';
		$data ='';
		for($i =0 ; $i < count($keys) ; $i++)
		{
			$key = $keys[$i];
			$data = $data.$key.'='.$sz_http_post_data[$key].'&';
		}
		$eol = "\r\n";
		$errno = 0;
		$errstr = '';
		$fid = fsockopen($sz_server, 80, $errno, $errstr, 90);

		if ($fid)
		{
			$http_request  = "POST /$sz_server_path HTTP/1.0\r\n";
			$http_request .= "Host: ".$sz_server."\r\n";
			$http_request .= "Content-Type: application/x-www-form-urlencoded; \r\n";
			$http_request .= "Content-Length: ".strlen($data)."\r\n";
			$http_request .= "User-Agent: SezWho/$sz_plugin_version.$sz_revision\r\n";
			$http_request .= "\r\n";
			$http_request .= $data;
			fwrite($fid, $http_request);
			$content = '';
			while (!feof($fid))
			{
				$content .= fgets($fid, 1160);
			}
			fclose($fid);
			$response =trim(substr($content,strpos($content,"CPRESPONSE")+10,strlen($content)));
			return $response;
		}
		return null;
	}


	/**
	* Processes the response received from the SezWho server on posting the sync data
	*
	* @param string $response
	* @return string
	*/
	function sz_process_response($response)
	{
		global $sz_db_type, $sz_log_base;

		$returned_values = explode('|', $response) ; // split at the commas
		$sz_unique_yk_scores = array();
		$sql	= '';

		if(trim($returned_values[0]) != "SUCCESS=N")
		{
			foreach($returned_values as $item)
			{
				$cols			= $this->sz_get_key_value_pairs_from_string_dictionary($item);
				$email 			= $cols['EMAIL'];
				$yk_score 		= $cols['YKSCORE'];
				$global_name 	= $cols['GLOBALNAME'];
				$encoded_email 	= $cols['ECRYPTED_EMAIL'];
				if($email != '')
				{
					/*
					$insquery 	= "INSERT INTO sz_email (email_address,yk_score,global_name, encoded_email)
						VALUES ('".$email."','".$yk_score."','".$global_name."', '".$encoded_email."')";
					$this->platform_wrapper->sz_execute_update_insert_query($insquery);
					*/

					$sql	= "	SELECT COUNT(*) AS email_count
								FROM sz_email
								WHERE email_address = '$email'";
					$email_count = $this->platform_wrapper->sz_get_column_value_in_first_row_for_query($sql, 'email_count');

					if ($email_count == 1)
					{ // this email already exists, hence update it
						$sql 	= "	UPDATE sz_email
									SET yk_score = '$yk_score' , global_name = '$global_name'
									WHERE email_address = '$email'" ;
					}
					else
					{ // this email does not exist, hence insert it
						$sql	= "	INSERT INTO sz_email (email_address, yk_score, global_name, encoded_email)
									VALUES ( '$email' , '$yk_score' , '$global_name', '$encoded_email')" ;
					}
					$this->platform_wrapper->sz_execute_update_insert_query($sql);
				}
			}

			//Compute and update comment score
			switch ($sz_db_type)
			{
				/*
				case 'mysql':
					$sql ="UPDATE sz_comment ,sz_email SET sz_comment.raw_score=(sz_email.yk_score-5)*10 WHERE sz_comment.exclude_flag='S' AND sz_comment.email_address=sz_email.email_address";
					break;*/
				case 'sqlite':
					$sql	= '	SELECT email_address, yk_score 
								FROM sz_email 
									GROUP BY(email_address)';
					$sz_unique_yk_scores = $this->platform_wrapper->sz_execute_select_query($sql);
					break;
				/*case 'oracle':
					$sql="UPDATE sz_comment szc SET raw_score = (((select  yk_score FROM sz_email WHERE sz_email.email_address=szc.email_address)-5)*10) WHERE szc.exclude_flag='S'";
					break;
				case 'firebird':
					$sql="UPDATE sz_comment SET sz_comment.raw_score=(sz_email.yk_score-5)*10 FROM sz_comment INNER JOIN sz_email ON sz_comment.email_address=sz_email.email_address WHERE sz_comment.exclude_flag='S'";
					break;


				case 'postgres':
					$sql="UPDATE sz_comment SET raw_score=(yk_score-5)*10 FROM sz_email WHERE sz_comment.email_address=sz_email.email_address AND sz_comment.exclude_flag='S'";
					break;
					*/
				case 'mssql':
					$sql	= "	UPDATE sz_comment 
								SET sz_comment.raw_score=(sz_email.yk_score-5)*10 
								FROM sz_comment 
								INNER JOIN sz_email 
									ON sz_comment.email_address=sz_email.email_address 
								WHERE sz_comment.exclude_flag='S'";
					break;				
				default:
					$sql	= "	UPDATE sz_comment szc 
								SET raw_score = (((SELECT yk_score FROM sz_email WHERE sz_email.email_address=szc.email_address)-5)*10) 
								WHERE szc.exclude_flag='S'";
			}
			
			if($sz_db_type=='sqlite')
			{
				foreach($sz_unique_yk_scores as $sz_unique_yk_score)
				{
					$sql	= '	UPDATE sz_comment 
								SET raw_score = (('.$sz_unique_yk_score['yk_score'].' - 5)*10)
								WHERE email_address= "'.$sz_unique_yk_score['email_address'].'"';
					$this->platform_wrapper->sz_execute_update_insert_query($sql);
				}
				unset($sz_unique_yk_scores);
			}
			else
			{
				$this->platform_wrapper->sz_execute_update_insert_query($sql);
			}

			switch ($sz_db_type)
			{
				case 'mysql':
				case 'oracle':
				case 'firebird':
					$sql	= "	UPDATE sz_comment
								SET comment_score = ( LOG($sz_log_base,raw_score) + 5 )
								WHERE exclude_flag='S' 
									AND sz_comment.raw_score > 1 ";
					break;
				case 'sqlite':
					sqlite_create_function($this->platform_wrapper->db_handle->db_connect_id, 'log', 'log', 2);
					$sql 	= "	UPDATE sz_comment
								SET comment_score = ( log(raw_score,$sz_log_base) + 5 )
								WHERE exclude_flag='S'
									AND sz_comment.raw_score > 1 ";
					break;
				case 'mssql':
					$sql	= "	UPDATE sz_comment
								SET comment_score = ( LOG(raw_score)/LOG($sz_log_base) + 5 )
								WHERE exclude_flag='S'
									AND sz_comment.raw_score > 1 ";
					break;
				case 'postgres':
					$sql	= "	UPDATE sz_comment
								SET comment_score = ( LOG($sz_log_base,CAST(raw_score AS NUMERIC)) + 5 )
								WHERE exclude_flag='S'
									AND sz_comment.raw_score > 1 ";
					break;
			}
			$this->platform_wrapper->sz_execute_update_insert_query($sql);


			switch ($sz_db_type)
			{
				case 'mysql':
				case 'sqlite':
				case 'oracle':
				case 'firebird':
				$sql	= "	UPDATE sz_comment
							SET comment_score = ( -1*LOG($sz_log_base,-1*raw_score) + 5 )
							WHERE exclude_flag='S'
								AND sz_comment.raw_score < -1 ";
				break;
				case 'sqlite':
					sqlite_create_function($this->platform_wrapper->db_handle->db_connect_id, 'log', 'log', 2);
					$sql	= "	UPDATE sz_comment
								SET comment_score = ( -1*log(-1*raw_score, $sz_log_base) + 5 )
								WHERE exclude_flag='S'
									AND sz_comment.raw_score < -1 ";
					break;
				case 'mssql':
					$sql	= "	UPDATE sz_comment
								SET comment_score = ( -1* (LOG(-1*raw_score)/LOG($sz_log_base)) + 5 )
								WHERE exclude_flag='S'
									AND sz_comment.raw_score < -1 ";
					break;
				case 'postgres':
					$sql	= "	UPDATE sz_comment
								SET comment_score = ( -1*LOG($sz_log_base,-1*CAST(raw_score AS NUMERIC)) + 5 )
								WHERE exclude_flag='S'
									AND sz_comment.raw_score < -1 ";
					break;
			}
			$this->platform_wrapper->sz_execute_update_insert_query($sql);

			$sql	= "	UPDATE sz_comment
						SET comment_score=5
						WHERE exclude_flag='S'
							AND sz_comment.raw_score >= -1
							AND sz_comment.raw_score <= 1";
			$this->platform_wrapper->sz_execute_update_insert_query($sql);

			// Fix the exclude flag
			$exculde_flag_update_query	= "	UPDATE sz_comment
											SET exclude_flag = NULL
											WHERE  exclude_flag ='S' ";
			$this->platform_wrapper->sz_execute_update_insert_query($exculde_flag_update_query);

			return '1';
		}
		else
		{
			$msgarr = explode('=',$returned_values[1]);
			$delete_comment_query	= "	DELETE
										FROM sz_comment
										WHERE  exclude_flag ='S'";
			$this->platform_wrapper->sz_execute_update_insert_query($delete_comment_query);
			return trim($msgarr[1]);
		}
	}

	

	/**
	* @ignore
	*/
	function sz_process_rating($post_id, $comment_id, $rater_email, $rating, $rating_type)
	{
		global $sz_server_url, $sz_plugin_version;
		global $sz_rating_count;
		global $sz_db_type;

		$sz_rating_count = 0;

		if('P'==$rating_type)
		{
			$sql	= "SELECT blog_id FROM sz_comment WHERE posting_id=-1 AND comment_id=$post_id AND content_type='P'";
		}
		elseif('C'==$rating_type)
		{
			$sql	= "SELECT blog_id FROM sz_comment WHERE comment_id=$comment_id AND posting_id=$post_id AND content_type='C'";
		}
		$blog_id= $this->platform_wrapper->sz_get_column_value_in_first_row_for_query($sql, 'blog_id') ;

		$blog_key = $this->platform_wrapper->sz_get_blog_key($blog_id);
		$site_key = $this->platform_wrapper->sz_get_site_key();


		if($site_key=='' || $site_key==null)
		{
			$postrating_ws_result = 'Status=Failure,ErrorMsgCode=NoSiteKey';
			return $postrating_ws_result ;
		}

		if($blog_key=='' || $blog_key==null)
		{
			$postrating_ws_result = 'Status=Failure,ErrorMsgCode=NoBlogKey';
			return $postrating_ws_result ;
		}

		$platform	= substr($sz_plugin_version, 0, 2) ;
		$version	= substr($sz_plugin_version, 2) ;

		if ($rating_type == 'P')
		{
			$email_query = "SELECT email_address FROM sz_comment WHERE posting_id=-1 AND comment_id='$post_id' AND content_type='P'" ;
		}
		elseif('C'==$rating_type)
		{
			$email_query = "SELECT email_address FROM sz_comment WHERE comment_id='$comment_id' AND posting_id='$post_id' AND content_type='C'" ;
		}
		$email_res = $this->platform_wrapper->sz_get_column_value_in_first_row_for_query($email_query, 'email_address') ;

		$sql = "SELECT encoded_email FROM sz_email WHERE email_address = '$email_res'" ;
		$email_res = $this->platform_wrapper->sz_get_column_value_in_first_row_for_query($email_query, 'encoded_email') ;


		if($email_res == $rater_email)
		{
			$postrating_ws_result = 'Status=Failure,ErrorMsgCode=SelfRating';
			return $postrating_ws_result ;
		}
		else
		{
			$webservice_url="/webservices/ykwebservice_front.php?method=postRating&site_key=$site_key&blog_id=$blog_id&blog_key=$blog_key&posting_id=$post_id&comment_id=$comment_id&rating=$rating&email_address=".rawurlencode($rater_email)."&plugin_version=$version&rating_type=$rating_type";
			$postrating_ws_result 	= $this->sz_http_post_2_server('',  $sz_server_url, $webservice_url, 80);
		}

		//return "$webservice_url";

		$result_arr = $this->sz_get_key_value_pairs_from_string_dictionary($postrating_ws_result);

		// update rater's yk score
		$rater_ykscore = $result_arr["YKScore"] ;
		if ($rater_ykscore != null)
		{
			$update_email_query = "	UPDATE sz_email SET yk_score =$rater_ykscore WHERE encoded_email = '$rater_email'" ;
			$this->platform_wrapper->sz_execute_update_insert_query($update_email_query);
		}


		$status	= $result_arr["Status"] ;
		if ($status == "Success")
		{
			if ($rater_ykscore == '')
			{
				$rater_ykscore = 5 ; // in this case setting rater yk-score to default score
			}

			if ($rating_type == 'P')
			{
				$blog_author_yk_score	= $result_arr["BlogAuthorYKScore"];
				$blog_author_email 		= $result_arr["BlogAuthorEmail"];

				if ($blog_author_yk_score != null)
				{
					$update_yk_score = "UPDATE sz_email SET yk_score=$blog_author_yk_score WHERE email_address='$blog_author_email'";
					$this->platform_wrapper->sz_execute_update_insert_query($update_yk_score);
				}

				if ($rater_ykscore)
				{
					$score_query	= "SELECT * FROM sz_comment WHERE posting_id=-1 AND comment_id =$post_id AND content_type='P'";
					$score_res 		= $this->platform_wrapper->sz_execute_select_query($score_query);

					if("anonymous" == $rater_email)
					{
						$anon_raw_score 		= $score_res[0]['anon_raw_score'];
						$anon_rating_count 		= $score_res[0]['anon_rating_count'];
						if(!$anon_raw_score)
						{
							$anon_raw_score 	= 0;
						}
						if(!$anon_rating_count)
						{
							$anon_rating_count 	= 0;
						}
						$new_anon_raw_score		= 5*($rating-5) + $anon_raw_score;
						$new_anon_rating_count 	= $anon_rating_count + 1;

						$post_score = $this->sz_apply_anonymous_score($new_anon_raw_score, $score_res[0]['comment_score']);

						$update_rater_ykscore_query = "	UPDATE sz_comment
														SET anon_raw_score=$new_anon_raw_score, anon_rating_count=$new_anon_rating_count 
														WHERE posting_id=-1 
															AND comment_id =$post_id 
															AND content_type='P'";
					}
					else
					{
						$raw_score 			= $score_res[0]['raw_score'];
						$new_raw_score 		= $rater_ykscore * ($rating - 5) + $raw_score;
						$new_rating_count 	= $score_res[0]['rating_count'] + 1;
						$post_score 		= $this->sz_get_content_score($new_raw_score);


						$update_rater_ykscore_query = "	UPDATE sz_comment
														SET comment_score = $post_score , raw_score=$new_raw_score, rating_count = $new_rating_count
														WHERE posting_id=-1 
															AND comment_id = '$post_id'
															AND content_type = 'P'";
						$post_score 		= $this->sz_apply_anonymous_score($score_res[0]['anon_raw_score'], $post_score);
					}
					$sz_rating_count = $score_res[0]['rating_count'] + $score_res[0]['anon_rating_count'] + 1;

					$this->platform_wrapper->sz_execute_update_insert_query($update_rater_ykscore_query );
					$postscore_formatted 	= number_format($post_score, "1");
					$postrating_ws_result 	= "Status=Success,Rating=$postscore_formatted" ;
				}
				else
				{
					$postrating_ws_result = "Status=Failure" ;
				} // rater

			}
			else
			{
				$commenter_yk_score		= $result_arr["CommenterYKscore"];
				$commenter_email		= $result_arr["CommenterEmail"];
				if($commenter_yk_score	!= null)
				{
					$update_yk_score = "UPDATE sz_email SET yk_score=$commenter_yk_score WHERE email_address='$commenter_email'";
					$this->platform_wrapper->sz_execute_update_insert_query($update_yk_score);
				}
				if($rater_ykscore != null)
				{
					$sql	= "	SELECT * FROM sz_comment WHERE comment_id=$comment_id AND posting_id=$post_id AND content_type='C'";
					$comment = $this->platform_wrapper->sz_execute_select_query($sql);

					if("anonymous" == $rater_email)
					{
						$anon_raw_score 	= $comment[0]['anon_raw_score'];
						$anon_rating_count 	= $comment[0]['anon_rating_count'];
						if(!$anon_raw_score)
						{
							$anon_raw_score = 0;
						}
						if(!$anon_rating_count)
						{
							$anon_rating_count = 0;
						}
						$new_anon_raw_score= 5*($rating-5) + $anon_raw_score;
						$new_anon_rating_count = $anon_rating_count + 1;

						$commentscore = $this->sz_apply_anonymous_score($new_anon_raw_score, $comment[0]['comment_score']);

						$update_comment_query	= "	UPDATE sz_comment
													SET anon_raw_score='$new_anon_raw_score' , anon_rating_count = '$new_anon_rating_count'
													WHERE comment_id='$comment_id'
														AND posting_id='$post_id'
														AND content_type='C'";
					}
					else
					{
						$raw_score 			= $comment[0]['raw_score'];
						$new_raw_score		= $rater_ykscore*($rating-5) + $raw_score;
						$new_rating_count 	= $comment[0]['rating_count'] + 1 ;
						$commentscore		= $this->sz_get_content_score($new_raw_score);

						$update_comment_query	= "	UPDATE sz_comment
													SET comment_score='$commentscore' , raw_score='$new_raw_score' , rating_count = '$new_rating_count'
													WHERE comment_id='$comment_id'
														AND posting_id='$post_id'
														AND content_type='C'";
						$commentscore 		= $this->sz_apply_anonymous_score($comment[0]['anon_raw_score'], $commentscore);
					}

					$sz_rating_count 		= $comment[0]['rating_count'] + $comment[0]['anon_rating_count'] + 1;
					$this->platform_wrapper->sz_execute_update_insert_query($update_comment_query);
					$commentscore_formatted = number_format($commentscore, "1");
					$postrating_ws_result 	= "Status=Success,Rating=$commentscore_formatted";



				}
				else
				{
					$postrating_ws_result = "Status=Failure" ;
				}
			} // if rating_type
		}
		else if ($status == 'Blocked' | $status == 'Denied')
		{
			$postrating_ws_result = "Status=$status" ;
		}
		return $postrating_ws_result ;

	}




	/**
	* Returns script to get star rating
	*
	* @param float $rating
	* @param string $widget
	* @return string
	*/
	function sz_get_star_rating($rating, $widget = "normal")
	{
		$temp_score 	= $rating ;
		$ratings_images = '' ;
		$comment_score 	= $temp_score / 2  ;
		$display_score 	= number_format($comment_score, 1) ;
		if("badge" == $widget)
		{
			$on_button_class 	= "cpPopupContentUserStarImgonBG";
			$half_button_class 	= "cpPopupContentUserStarImghalfBG";
			$off_button_class 	= "cpPopupContentUserStarImgoffBG";
		}
		else
		{
			$on_button_class 	= "szRCStarImgon szRCStarImgonCustom";
			$half_button_class 	= "szRCStarImghalf szRCStarImghalfCustom";
			$off_button_class 	= "szRCStarImgoff szRCStarImgoffCustom";
		}
		$full_img = "<button title='Star Power: ".$display_score ."' type='button' class='".$on_button_class."'></button>";
		$half_img = "<button title='Star Power: ".$display_score ."' type='button' class='".$half_button_class."'></button>";
		$zero_img = "<button title='Star Power: ".$display_score ."' type='button' class='".$off_button_class."'></button>";

		if ($comment_score < 0.25)
		{
			$ratings_images = $zero_img.$zero_img.$zero_img.$zero_img.$zero_img ;
		}
		else if ($comment_score >= 0.25 && $comment_score < 0.75)
		{
			$ratings_images = $half_img.$zero_img.$zero_img.$zero_img.$zero_img ;
		}
		else if ($comment_score >= 0.75 && $comment_score < 1.25)
		{
			$ratings_images = $full_img.$zero_img.$zero_img.$zero_img.$zero_img ;
		}
		else if ($comment_score >= 1.25 && $comment_score < 1.75)
		{
			$ratings_images = $full_img.$half_img.$zero_img.$zero_img.$zero_img ;
		}
		else if ($comment_score >= 1.75 && $comment_score < 2.25)
		{
			$ratings_images = $full_img.$full_img.$zero_img.$zero_img.$zero_img ;
		}
		else if ($comment_score >= 2.25 && $comment_score < 2.75)
		{
			$ratings_images = $full_img.$full_img.$half_img.$zero_img.$zero_img ;
		}
		else if ($comment_score >= 2.75 && $comment_score < 3.25)
		{
			$ratings_images = $full_img.$full_img.$full_img.$zero_img.$zero_img ;
		}
		else if ($comment_score >= 3.25 && $comment_score < 3.75)
		{
			$ratings_images = $full_img.$full_img.$full_img.$half_img.$zero_img ;
		}
		else if ($comment_score >= 3.75 && $comment_score < 4.25)
		{
			$ratings_images = $full_img.$full_img.$full_img.$full_img.$zero_img ;
		}
		else if ($comment_score >= 4.25 && $comment_score < 4.75)
		{
			$ratings_images = $full_img.$full_img.$full_img.$full_img.$half_img ;
		}
		else if ($comment_score >= 4.75)
		{
			$ratings_images = $full_img.$full_img.$full_img.$full_img.$full_img ;
		}

		return $ratings_images ;
	}


	/**
	* Returns the required queries to create cache tables for this plugin
	*
	* @return array index: query type, value = query
	*/
	function sz_fetch_cache_table_queries()
	{
		global $sz_db_type;
		//Initialize the Diff DB Arrays
		$mysql		= array();
		$sqlite		= array();
		$postgres	= array();
		$firebird	= array();
		$oracle		= array();
		$mssql		= array();

		$mysql = array(
			'sz_site'							=> "
													CREATE TABLE sz_site (
														site_key VARCHAR(32) NOT NULL DEFAULT '',
														plugin_version VARCHAR(6) NOT NULL,
														site_url VARCHAR(255),
														rating_verification VARCHAR(16),
													CONSTRAINT site_pk PRIMARY KEY (site_key)
													) ENGINE=InnoDB DEFAULT CHARSET=UTF8
												   ",
			'sz_blog'							=> '
													CREATE TABLE sz_blog (
														blog_id int(8) NOT NULL,
														blog_key VARCHAR(32),
														blog_url VARCHAR(255) ,
														blog_title VARCHAR(255) ,
														blog_subject VARCHAR(255) ,
														display_template VARCHAR(16) ,
														language VARCHAR(16) ,
														site_key VARCHAR(32) NOT NULL,
													CONSTRAINT blog_pk PRIMARY KEY (blog_id),
													INDEX site_key_index (site_key),
													FOREIGN KEY (site_key) REFERENCES sz_site(site_key),
													INDEX blog_idx2(blog_key)
													) ENGINE=InnoDB DEFAULT CHARSET=UTF8
											       ',
			'sz_email'							=> '
													CREATE TABLE sz_email (
														email_address VARCHAR(255) NOT NULL,
														yk_score FLOAT ,
														global_name VARCHAR(255) ,
														encoded_email VARCHAR(255),
													CONSTRAINT email_pk PRIMARY KEY (email_address)
													) ENGINE=InnoDB DEFAULT CHARSET=UTF8
												   ',

			'sz_comment'						=> '
													CREATE TABLE sz_comment (
														blog_id INT(8) NOT NULL,
														posting_id INT(8) NOT NULL,
														comment_id INT(8) NOT NULL,
														content_type VARCHAR(1) NOT NULL,
														creation_date DATE,
														comment_rating FLOAT ,
														comment_score FLOAT ,
														raw_score FLOAT ,
														rating_count int(8) DEFAULT 0,
														anon_raw_score FLOAT ,
														anon_rating_count int(8) DEFAULT 0,
														email_address VARCHAR(255) NOT NULL,
														exclude_flag VARCHAR(1),
													CONSTRAINT comment_pk PRIMARY KEY (blog_id, posting_id, comment_id, content_type)
													) ENGINE=InnoDB DEFAULT CHARSET=UTF8
													',
			'sz_blog_user'						=> '
													CREATE TABLE sz_blog_user (
														blog_id int(8) NOT NULL,
														screen_name VARCHAR(255) NOT NULL,
														email_address VARCHAR(255) NOT NULL,
														yk_score FLOAT ,
													CONSTRAINT email_pk PRIMARY KEY (blog_id, email_address),
													CONSTRAINT email_uk UNIQUE (screen_name),
													INDEX blog_id_index (blog_id),
													FOREIGN KEY (blog_id) REFERENCES sz_blog(blog_id),
													INDEX email_address_index (email_address),
													FOREIGN KEY (email_address) REFERENCES sz_email(email_address)
													) ENGINE=InnoDB DEFAULT CHARSET=UTF8
													',
			'sz_post'							=> '
													CREATE TABLE  sz_post (
														blog_id int(11) NOT NULL,
														posting_id int(11) NOT NULL,
														creation_date DATE NOT NULL,
														post_score FLOAT DEFAULT NULL,
														raw_score FLOAT DEFAULT NULL,
														rating_count int(8) DEFAULT 0,
														anon_raw_score FLOAT DEFAULT NULL,
														anon_rating_count int(8) DEFAULT 0,
														email_address VARCHAR(255) NOT NULL,
														exclude_flag VARCHAR(1),
													PRIMARY KEY  (blog_id,posting_id)
													) ENGINE=InnoDB DEFAULT CHARSET=UTF8
												   '
					);

		$sqlite = array(

			'sz_site'							=> "
													CREATE TABLE sz_site (
														site_key VARCHAR(32) NOT NULL DEFAULT '' ,
														plugin_version VARCHAR(6) NOT NULL ,
														site_url VARCHAR(255) ,
														rating_verification VARCHAR(16) ,
													CONSTRAINT site_pk PRIMARY KEY (site_key)
													)
												   ",
			'sz_blog'							=> '
													CREATE TABLE sz_blog (
														blog_id INTEGER NOT NULL,
														blog_key VARCHAR(32),
														blog_url VARCHAR(255) ,
														blog_title VARCHAR(255) ,
														blog_subject VARCHAR(255) ,
														display_template VARCHAR(16) ,
														language VARCHAR(16) ,
														site_key VARCHAR(32) NOT NULL,
													CONSTRAINT blog_pk PRIMARY KEY (blog_id),
													FOREIGN KEY (site_key) REFERENCES sz_site(site_key)
													)
											       ',
			'sz_blog_site_key_index'			=> 'CREATE INDEX site_key_index ON sz_blog(site_key)',
			'sz_blog_blog_idx2'					=> 'CREATE INDEX blog_idx2 ON sz_blog(blog_key)',
			'sz_email'							=> '
													CREATE TABLE sz_email (
														email_address VARCHAR(255) NOT NULL,
														yk_score FLOAT ,
														global_name VARCHAR(255) ,
														encoded_email VARCHAR(255),
													CONSTRAINT email_pk PRIMARY KEY (email_address)
													)
												   ',

			'sz_comment'						=> '
													CREATE TABLE sz_comment (
														blog_id INTEGER NOT NULL,
														posting_id INTEGER NOT NULL,
														comment_id INTEGER NOT NULL,
														content_type VARCHAR(1) NOT NULL,
														creation_date TIMESTAMP,
														comment_rating FLOAT ,
														comment_score FLOAT ,
														raw_score FLOAT ,
														rating_count INTEGER DEFAULT 0,
														anon_raw_score FLOAT ,
														anon_rating_count INTEGER DEFAULT 0,
														email_address VARCHAR(255) NOT NULL,
														exclude_flag VARCHAR(1),
													CONSTRAINT comment_pk PRIMARY KEY (blog_id, posting_id, comment_id, content_type)
	    											)
													',
			'sz_blog_user'						=> '
													CREATE TABLE sz_blog_user (
														blog_id INTEGER NOT NULL,
														screen_name VARCHAR(255) NOT NULL,
														email_address VARCHAR(255) NOT NULL,
														yk_score FLOAT ,
													CONSTRAINT blog_email_pk PRIMARY KEY (blog_id, email_address),
													CONSTRAINT email_uk UNIQUE (screen_name),
													FOREIGN KEY (blog_id) REFERENCES sz_blog(blog_id),
													FOREIGN KEY (email_address) REFERENCES sz_email(email_address)
	    											)
													',
			'sz_blog_user_blog_id_index'		=> 'CREATE INDEX blog_id_index ON sz_blog_user(blog_id)',
			'sz_blog_email_address_index'		=> 'CREATE INDEX email_address_index ON sz_blog_user(email_address)',
			'sz_post'							=> '
													CREATE TABLE  sz_post (
														blog_id INTEGER NOT NULL,
														posting_id INTEGER NOT NULL,
														creation_date TIMESTAMP NOT NULL,
														post_score FLOAT DEFAULT NULL,
														raw_score FLOAT DEFAULT NULL,
														rating_count INTEGER DEFAULT 0,
														anon_raw_score FLOAT DEFAULT NULL,
														anon_rating_count INTEGER DEFAULT 0,
														email_address VARCHAR(255) NOT NULL,
														exclude_flag VARCHAR(1),
													PRIMARY KEY  (blog_id,posting_id)
													)
												   '
						);

			$postgres = array(
			'sz_site'							=>	"
													CREATE TABLE sz_site (
														site_key VARCHAR(32) NOT NULL DEFAULT '' ,
														plugin_version VARCHAR(6) NOT NULL ,
														site_url VARCHAR(255) ,
														rating_verification VARCHAR(16) ,
													CONSTRAINT site_pk PRIMARY KEY (site_key)
													)
													",
			'sz_blog'							=> '
													CREATE TABLE sz_blog (
														blog_id INTEGER NOT NULL,
														blog_key VARCHAR(32),
														blog_url VARCHAR(255) ,
														blog_title VARCHAR(255) ,
														blog_subject VARCHAR(255) ,
														display_template VARCHAR(16) ,
														language VARCHAR(16) ,
														site_key VARCHAR(32) NOT NULL,
													CONSTRAINT blog_pk PRIMARY KEY (blog_id),
													FOREIGN KEY (site_key) REFERENCES sz_site(site_key)
													)
											       ',
			'sz_blog_site_key_index'			=> 'CREATE INDEX site_key_index ON sz_blog(site_key)',
			'sz_blog_blog_idx2'					=> 'CREATE INDEX blog_idx2 ON sz_blog(blog_key)',
			'sz_email'							=> '
													CREATE TABLE sz_email (
														email_address VARCHAR(255) NOT NULL,
														yk_score FLOAT ,
														global_name VARCHAR(255) ,
														encoded_email VARCHAR(255),
													CONSTRAINT email_pk PRIMARY KEY (email_address)
													)
												   ',

			'sz_comment'						=> '
													CREATE TABLE sz_comment (
														blog_id INTEGER NOT NULL,
														posting_id INTEGER NOT NULL,
														comment_id INTEGER NOT NULL,
														content_type VARCHAR(1) NOT NULL,
														creation_date TIMESTAMP,
														comment_rating FLOAT ,
														comment_score FLOAT ,
														raw_score FLOAT ,
														rating_count INTEGER DEFAULT 0,
														anon_raw_score FLOAT ,
														anon_rating_count INTEGER DEFAULT 0,
														email_address VARCHAR(255) NOT NULL,
													exclude_flag VARCHAR(1),
													CONSTRAINT comment_pk PRIMARY KEY (blog_id, posting_id, comment_id, content_type)
													)
													',
			'sz_blog_user'						=> '
													CREATE TABLE sz_blog_user (
														blog_id INTEGER NOT NULL,
														screen_name VARCHAR(255) NOT NULL,
														email_address VARCHAR(255) NOT NULL,
														yk_score FLOAT ,
													CONSTRAINT blog_email_pk PRIMARY KEY (blog_id, email_address),
													CONSTRAINT email_uk UNIQUE (screen_name),
													FOREIGN KEY (blog_id) REFERENCES sz_blog(blog_id),
													FOREIGN KEY (email_address) REFERENCES sz_email(email_address)
													)
													',
			'sz_blog_user_blog_id_index'		=> 'CREATE INDEX blog_id_index ON sz_blog_user(blog_id)',
			'sz_blog_email_address_index'		=> 'CREATE INDEX email_address_index ON sz_blog_user(email_address)',
			'sz_post'							=> '
													CREATE TABLE  sz_post (
														blog_id INTEGER NOT NULL,
														posting_id INTEGER NOT NULL,
														creation_date TIMESTAMP NOT NULL,
														post_score FLOAT DEFAULT NULL,
														raw_score FLOAT DEFAULT NULL,
														rating_count INTEGER DEFAULT 0,
														anon_raw_score FLOAT DEFAULT NULL,
														anon_rating_count INTEGER DEFAULT 0,
														email_address VARCHAR(255) NOT NULL,
														exclude_flag VARCHAR(1),
													PRIMARY KEY  (blog_id,posting_id)
													)
												   ',
			'sz_from_unixtime_udf'				=> "CREATE OR REPLACE FUNCTION FROM_UNIXTIME(INTEGER)
													RETURNS ABSTIME AS '
													SELECT ABSTIME($1) AS RESULT
													'LANGUAGE 'sql'
													"
						);
			$firebird = array(
			'sz_site'							=>	"
													CREATE TABLE sz_site (
														site_key  varchar(32)  CHARACTER SET NONE DEFAULT '' NOT NULL,
														plugin_version varchar(6)  CHARACTER SET NONE NOT NULL ,
														site_url VARCHAR(255)  CHARACTER SET NONE,
														rating_verification VARCHAR(16)   CHARACTER SET NONE,
													PRIMARY KEY (site_key)
													)
													",
			'sz_blog'							=> '
													CREATE TABLE sz_blog(
														blog_id INTEGER NOT NULL,
														blog_key  VARCHAR(255)  CHARACTER SET NONE,
														blog_url  VARCHAR(255)  CHARACTER SET NONE,
														blog_title  VARCHAR(255)  CHARACTER SET NONE,
														blog_subject  VARCHAR(255)  CHARACTER SET NONE,
														display_template  VARCHAR(16)  CHARACTER SET NONE,
														language  VARCHAR(16)  CHARACTER SET NONE,
														site_key  VARCHAR(32) CHARACTER SET NONE NOT NULL,
													PRIMARY KEY (blog_id)
													)
													',

			'sz_blog_site_key_index'			=> 'CREATE INDEX site_key_index ON sz_blog(site_key)',
			'sz_blog_references'				=> '
													ALTER TABLE sz_blog
													ADD FOREIGN KEY (site_key) REFERENCES sz_site(site_key) ON	DELETE CASCADE ON UPDATE CASCADE
													',
			'sz_blog_blog_idx2'					=> 'CREATE INDEX blog_idx2 ON sz_blog(blog_key)',
			'sz_email'							=> '
													CREATE TABLE sz_email (
														email_address VARCHAR(255) CHARACTER SET NONE NOT NULL,
														yk_score FLOAT,
														global_name VARCHAR(255)  CHARACTER SET NONE,
														encoded_email VARCHAR(255) CHARACTER SET NONE,
													PRIMARY KEY (email_address))
												   ',

			'sz_comment'						=> '
													CREATE TABLE sz_comment (
														blog_id INTEGER NOT NULL,
														posting_id INTEGER NOT NULL,
														comment_id INTEGER NOT NULL,
														content_type VARCHAR(1) NOT NULL,
														creation_date TIMESTAMP,
														comment_rating FLOAT,
														comment_score FLOAT,
														raw_score FLOAT,
														rating_count INTEGER DEFAULT 0,
														anon_raw_score FLOAT ,
														anon_rating_count INTEGER DEFAULT 0,
														email_address VARCHAR(255)  CHARACTER SET NONE NOT NULL,
														exclude_flag VARCHAR(1) CHARACTER SET NONE,
													PRIMARY KEY (blog_id, posting_id, comment_id, content_type))
													',
			'sz_blog_user'						=> '
													CREATE TABLE sz_blog_user (
														blog_id INTEGER NOT NULL,
														screen_name VARCHAR(255) CHARACTER SET NONE NOT NULL,
														email_address VARCHAR(255) CHARACTER SET NONE NOT NULL,
														yk_score FLOAT ,
													PRIMARY KEY (blog_id, email_address),
													UNIQUE (screen_name))
													',
			'sz_blog_user_blog_id_index'		=> 'CREATE INDEX blog_id_index ON sz_blog_user(blog_id)',
			'sz_blog_user_references'			=> "
													ALTER TABLE sz_blog_user
													ADD FOREIGN KEY (blog_id) REFERENCES sz_blog(blog_id) ON DELETE CASCADE ON UPDATE CASCADE
													",
			'sz_blog_email_address_index'		=> 'CREATE INDEX email_address_index ON sz_blog_user(email_address)',
			'sz_blog_user_references2'			=> "
													ALTER TABLE sz_blog_user
													ADD FOREIGN KEY (email_address) REFERENCES sz_email(email_address) ON DELETE CASCADE ON UPDATE CASCADE
													",
			'sz_post'							=> '
													CREATE TABLE  sz_post (
														blog_id INTEGER NOT NULL,
														posting_id INTEGER NOT NULL,
														creation_date TIMESTAMP NOT NULL,
														post_score FLOAT ,
														raw_score FLOAT ,
														rating_count INTEGER DEFAULT 0 NOT NULL,
														anon_raw_score FLOAT ,
														anon_rating_count INTEGER DEFAULT 0 NOT NULL,
														email_address varchar(255)  CHARACTER SET NONE NOT NULL,
														exclude_flag VARCHAR(1) CHARACTER SET NONE,
													PRIMARY KEY  (blog_id,posting_id)
													)
												   ',
			'sz_log_function'					=> "
													DECLARE EXTERNAL FUNCTION log DOUBLE PRECISION, DOUBLE PRECISION
													RETURNS DOUBLE PRECISION BY VALUE
													ENTRY_POINT 'IB_UDF_log' MODULE_NAME 'ib_udf'
													"
						);

			$oracle = array(
			'sz_site'							=> "
													CREATE TABLE sz_site (
														site_key VARCHAR2(32) DEFAULT '' NOT NULL ,
														plugin_version VARCHAR2(6) NOT NULL ,
														site_url		VARCHAR2(255) ,
														rating_verification VARCHAR2(16) ,
													CONSTRAINT site_pk PRIMARY KEY (site_key)
													)
												   ",
			'sz_blog'							=> '
													CREATE TABLE sz_blog (
														blog_id NUMBER(8) NOT NULL,
														blog_key VARCHAR2(32),
														blog_url VARCHAR2(255) ,
														blog_title VARCHAR2(255) ,
														blog_subject VARCHAR2(255) ,
														display_template VARCHAR2(16) ,
														language VARCHAR2(16) ,
														site_key VARCHAR2(32) NOT NULL,
													CONSTRAINT blog_pk PRIMARY KEY (blog_id),
													FOREIGN KEY (site_key) REFERENCES sz_site(site_key)
													)
											       ',
			'sz_blog_site_key_index'			=> 'CREATE INDEX site_key_index ON sz_blog(site_key)',
			'sz_blog_blog_idx2'					=> 'CREATE INDEX blog_idx2 ON sz_blog(blog_key)',
			'sz_email'							=> '
													CREATE TABLE sz_email (
														email_address VARCHAR2(255) NOT NULL,
														yk_score FLOAT ,
														global_name VARCHAR2(255) ,
														encoded_email VARCHAR2(255),
													CONSTRAINT email_pk PRIMARY KEY (email_address)
													)
												   ',

			'sz_comment'						=> '
													CREATE TABLE sz_comment (
														blog_id NUMBER(8) NOT NULL,
														posting_id NUMBER(8) NOT NULL,
														comment_id NUMBER(8) NOT NULL,
														content_type VARCHAR2(1) NOT NULL,
														creation_date varchar2(20),
														comment_rating FLOAT ,
														comment_score FLOAT ,
														raw_score FLOAT ,
														rating_count NUMBER(8) DEFAULT 0,
														anon_raw_score FLOAT ,
														anon_rating_count NUMBER(8) DEFAULT 0,
														email_address VARCHAR2(255) NOT NULL,
														exclude_flag VARCHAR2(1),
													CONSTRAINT comment_pk PRIMARY KEY (blog_id, posting_id, comment_id, content_type)
													)
													',
			'sz_blog_user'						=> '
													CREATE TABLE sz_blog_user (
														blog_id NUMBER(8) NOT NULL,
														screen_name VARCHAR2(255) NOT NULL,
														email_address VARCHAR2(255) NOT NULL,
														yk_score FLOAT ,
													CONSTRAINT blog_email_pk PRIMARY KEY (blog_id, email_address),
													CONSTRAINT email_uk UNIQUE (screen_name),
													FOREIGN KEY (blog_id) REFERENCES sz_blog(blog_id),
													FOREIGN KEY (email_address) REFERENCES sz_email(email_address)
													)
													',
			'sz_blog_user_blog_id_index'		=> 'CREATE INDEX blog_id_index ON sz_blog_user(blog_id)',
			'sz_blog_email_address_index'		=> 'CREATE INDEX email_address_index ON sz_blog_user(email_address)',
			'sz_post'							=> '
													CREATE TABLE  sz_post (
														blog_id NUMBER(11) NOT NULL,
														posting_id NUMBER(11) NOT NULL,
														creation_date varchar2(20) NOT NULL,
														post_score FLOAT DEFAULT NULL,
														raw_score FLOAT DEFAULT NULL,
														rating_count NUMBER(8) DEFAULT 0,
														anon_raw_score FLOAT DEFAULT NULL,
														anon_rating_count NUMBER(8) DEFAULT 0,
														email_address VARCHAR2(255) NOT NULL,
														exclude_flag VARCHAR2(1),
													PRIMARY KEY  (blog_id,posting_id)
													)
												   '
						);
			$mssql = array(
			'sz_site'							=> "
													CREATE TABLE sz_site(
														site_key VARCHAR(32) NOT NULL,
														plugin_version VARCHAR(6) NOT NULL,
														site_url VARCHAR(255) NULL,
														rating_verification VARCHAR(16) NULL
													) ON [PRIMARY]
												   ",
			'sz_constraint_site_pk'				=> "
													ALTER TABLE sz_site
													ADD CONSTRAINT site_pk PRIMARY KEY CLUSTERED (site_key) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
													",
			'sz_blog'							=> '
													CREATE TABLE sz_blog(
														blog_id BIGINT NOT NULL,
														blog_key VARCHAR(32) NULL,
														blog_url VARCHAR(255) NULL,
														blog_title VARCHAR(255) NULL,
														blog_subject VARCHAR(255) NULL,
														display_template VARCHAR(16) NULL,
														language VARCHAR(16) NULL,
														site_key VARCHAR(32) NOT NULL
													) ON [PRIMARY]
											       ',
			'sz_constraint_blog_pk'				=> '
													ALTER TABLE sz_blog
													ADD CONSTRAINT blog_pk PRIMARY KEY CLUSTERED (blog_id) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
													',
			'sz_site_key_index'					=> '
													CREATE NONCLUSTERED INDEX site_key_index ON sz_blog
													(blog_key) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
													',
			'sz_blog_idx2'						=> '
													CREATE NONCLUSTERED INDEX blog_idx2 ON sz_blog
													(blog_key) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
													',
			'sz_constraint_sz_blog'				=> '
													ALTER TABLE sz_blog
													ADD CONSTRAINT FK_sz_blog_sz_site FOREIGN KEY (site_key) REFERENCES sz_site(site_key) ON UPDATE NO ACTION ON DELETE NO ACTION
													',
			'sz_email'							=> '
													CREATE TABLE sz_email (
														email_address VARCHAR(255) NOT NULL,
														yk_score FLOAT ,
														global_name VARCHAR(255) ,
														encoded_email VARCHAR(255)
													) ON [PRIMARY]
												   ',
			'sz_email_constraint'				=> '
													ALTER TABLE sz_email
													ADD CONSTRAINT email_pk PRIMARY KEY CLUSTERED (email_address) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
													',

			'sz_comment'						=> '
													CREATE TABLE sz_comment (
														blog_id BIGINT NOT NULL,
														posting_id BIGINT NOT NULL,
														comment_id BIGINT NOT NULL,
														content_type VARCHAR(1) NOT NULL,
														creation_date DATETIME,
														comment_rating FLOAT ,
														comment_score FLOAT ,
														raw_score FLOAT ,
														rating_count BIGINT DEFAULT 0,
														anon_raw_score FLOAT ,
														anon_rating_count BIGINT DEFAULT 0,
														email_address VARCHAR(255) NOT NULL,
														exclude_flag VARCHAR(1)
													) ON [PRIMARY]
													',
			'sz_comment_constraint'				=> '
													ALTER TABLE sz_comment
													ADD CONSTRAINT comment_pk PRIMARY KEY CLUSTERED (blog_id, posting_id, comment_id, content_type) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
													',
			'sz_blog_user'						=> '
													CREATE TABLE sz_blog_user (
														blog_id BIGINT NOT NULL,
														screen_name VARCHAR(255) NOT NULL,
														email_address VARCHAR(255) NOT NULL,
														yk_score FLOAT
													) ON [PRIMARY]
													',
			'sz_blog_user_constraint'			=> '
													ALTER TABLE sz_blog_user
													ADD CONSTRAINT PK_sz_blog_user PRIMARY KEY CLUSTERED(blog_id) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
													',
			'sz_blog_user_blog_id_index'		=> '
													ALTER TABLE sz_blog_user
													ADD CONSTRAINT blog_id FOREIGN KEY	(blog_id) REFERENCES sz_blog ( blog_id ) ON UPDATE NO ACTION
													ON DELETE NO ACTION
													',
			'sz_blog_email_address_index'		=> '
													ALTER TABLE sz_blog_user
													ADD CONSTRAINT email_address FOREIGN KEY ( email_address ) REFERENCES sz_email	( email_address )
													ON UPDATE NO ACTION ON DELETE NO ACTION
													',
			'sz_post'							=> '
													CREATE TABLE sz_post (
														blog_id BIGINT NOT NULL,
														posting_id BIGINT NOT NULL,
														creation_date DATETIME NOT NULL,
														post_score FLOAT DEFAULT NULL,
														raw_score FLOAT DEFAULT NULL,
														rating_count BIGINT DEFAULT 0,
														anon_raw_score FLOAT DEFAULT NULL,
														anon_rating_count BIGINT DEFAULT 0,
														email_address VARCHAR(255) NOT NULL,
														exclude_flag VARCHAR(1)
													) ON [PRIMARY]
												   ',
			'sz_from_unixtime_udf'				=> "
													CREATE FUNCTION FROM_UNIXTIME(@unixtime int)
													RETURNS datetime AS
													begin
													declare @deltaGMT int, @date datetime
													exec master.dbo.xp_regread 'HKEY_LOCAL_MACHINE',
													'SYSTEM\CurrentControlSet\Control\TimeZoneInformation',
													'ActiveTimeBias', @deltaGMT OUT
													set @date=(SELECT dateadd(second, @unixtime, dateadd(minute, -@deltaGMT, '1970-01-01 00:00:00.0')))
													return @date
													end
													"

						);

		return $$sz_db_type; // note two dollar prefixsigns. For e.g. if $sz_db_type='postgres', $$sz_db_type = $($sz_db_type) = $postgres
	}


}
?>