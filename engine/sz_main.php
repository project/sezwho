<?php

/**
* @package SezWho
* @copyright (c) 2008 sezwho.com
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/


require_once($sz_plugin_path.'/engine/sz_functions_lib.php');

/**
* Main class for platform-SezWho interface API
* 
* @package SezWho
*/

class sz_main
{
	/**
	* @ignore 
	*/
	var $platform_wrapper;
	/**
	* @ignore 
	*/
	var $sz_functions_lib;

	
	function sz_main($platform_wrapper)
	{
		$this->platform_wrapper	= $platform_wrapper;
		$this->sz_functions_lib	= new sz_functions_lib($platform_wrapper);
	}

	
	/**
	* The platform must call this method to install SezWho.
	* 
	* @return string '1' on success else errorcode
	*/
	function sz_install()
	{
		global $sz_plugin_version, $sz_server_url, $sz_site_url;

		// setup the database
		$attempt=$this->sz_functions_lib->sz_setup_db();
		if ($attempt != '1')
		{
			return $attempt;
		}

		$site_key	= $this->platform_wrapper->sz_get_site_key();
		$response 	= $this->sz_functions_lib->sz_http_post_2_server('',  $sz_server_url, "/webservices/ykinstallplugin.php?site_key=$site_key&pluginversion=$sz_plugin_version&remoteurl=".urlencode($sz_site_url), 80);
		$result_arr	= $this->sz_functions_lib->sz_get_key_value_pairs_from_string_dictionary($response);

		if($result_arr['SUCCESS'] == 'Y')
		{
			//update the plugin database
			if($result_arr['SITEKEY'])
			{
				//insert site detail
				$site_query = "INSERT INTO sz_site (site_key,plugin_version,site_url)
					VALUES('".$result_arr['SITEKEY']."','$sz_plugin_version','$sz_site_url')";
			}
			else
			{
				//update site details when plugin is reinstalled
				$site_query = "UPDATE sz_site
					SET plugin_version ='$sz_plugin_version'
					WHERE site_key ='$site_key'";
			}
			$this->platform_wrapper->sz_execute_update_insert_query($site_query);
			return '1';
		}
		else
		{
			return 'SZ_ERR_SERVER_ERROR';
		}
	}

	
	/**
	* Activates the blog key.
	*
	* @param string $blog_key blog key obtained from SezWho.com
	* @return string '1' on success else errorcode
	*/
	function sz_activate_blog_key($blog_key, $blog_id=0)
	{
		global $sz_server_url, $sz_site_url;

		$site_key 		= $this->platform_wrapper->sz_get_site_key();
		$blog_title 	= $this->platform_wrapper->sz_get_blog_title($blog_id);
		$blog_url 		= $this->platform_wrapper->sz_get_blog_url($blog_id);
		$blog_subject 	= $this->platform_wrapper->sz_get_blog_subject($blog_id);
		$homeurl		= $sz_site_url;

		$webservice_url = "/webservices/ykactivateblogkey.php?sitekey=$site_key&blogtitle=".urlencode($blog_title)."&blogid=$blog_id&blogkey=$blog_key&blogurl=".urlencode($blog_url)."&homeurl=".urlencode($homeurl);
		$response = $this->sz_functions_lib->sz_http_post_2_server('',  $sz_server_url,$webservice_url , 80);
		$result_arr=$this->sz_functions_lib->sz_get_key_value_pairs_from_string_dictionary($response);


		if ($result_arr['SUCCESS'] == "N")
		{
			return $result_arr["ERRORMSGCODE"];
		}


		$blog_query = "SELECT COUNT(*) AS bcount
			FROM sz_blog
			WHERE blog_key='$blog_key'";
		$blog_num=$this->platform_wrapper->sz_get_column_value_in_first_row_for_query($blog_query,"bcount");

		if($blog_num == 0)
		{
			$insert_blog_query = "INSERT INTO sz_blog ( blog_id , blog_key , blog_url , blog_title ,blog_subject , site_key)
				VALUES ('$blog_id' , '$blog_key' , '$blog_url' , '$blog_title' ,'$blog_subject' , '$site_key')";
			$this->platform_wrapper->sz_execute_update_insert_query($insert_blog_query);
		}
		return '1';
	}

	
	/**
	* Synchronizes the platform content with SezWho
	*
	* @param int $blog_id
	* @return 
	*/
	function sz_synchronize($blog_id=0,$unsynchroinzed_content_fetching_query_param)
	{
		$attempt	= $this->sz_functions_lib->sz_synchronize_content($blog_id, $unsynchroinzed_content_fetching_query_param);
		return $attempt;
	}
	
	
	/**
	* Posts the content to SezWho
	*
	* @param mixed $content This parameter would be passed to the callback sz_get_*** methods in wrapper class to extract content attributes
	* @return string '1' on success else errorcode
	*/
	function sz_post_content(&$content)
	{
		global $sz_plugin_version, $sz_server_url, $sz_db_type;
		global $sz_content_title_length, $sz_content_intro_length, $sz_context_title_length, $sz_context_intro_length;

		
		if(!$this->platform_wrapper->sz_is_content_approved($content))
		{
			return 'SZ_ERR_UNAPPROVED_CONTENT';
		}
		
		if($this->sz_functions_lib->sz_is_content_posted_2_sz($content))
		{
			return 'SZ_ERR_CONTENT_ALREADY_POSTED_2_SZ';
		}

		$content_author_email	= $this->platform_wrapper->sz_get_content_author_email($content);
		if($content_author_email == '' || $content_author_email == null)
		{
			return 'SZ_ERR_NO_CONTENT_AUTHOR_EMAIL';
		}

		

		$version				= substr($sz_plugin_version, 2) ;
		$blog_id 				= $this->platform_wrapper->sz_get_content_blog_id($content);
		$site_key				= $this->platform_wrapper->sz_get_site_key();
		$blog_key				= $this->platform_wrapper->sz_get_blog_key($blog_id);
		if($blog_id=='' || $blog_id==null)
		{
			$blog_id			= 0;
		}

		$content_type			= strtoupper($this->platform_wrapper->sz_get_content_type($content));
		$content_id				= $this->platform_wrapper->sz_get_content_id($content);
		$content_title			= substr($this->platform_wrapper->sz_get_content_title($content), 0, $sz_content_title_length);
		$content_intro			= substr($this->platform_wrapper->sz_get_content_intro($content), 0, $sz_content_intro_length);
		$content_url			= $this->platform_wrapper->sz_get_content_url($content);
		$content_author_url		= $this->platform_wrapper->sz_get_content_author_url($content);

		$context_id				= $this->platform_wrapper->sz_get_context_id($content);

		if('-1'==$context_id)
		{
			$webservice_url	= "/webservices/ykwebservice_front.php?method=postContent&contentType=$content_type&site_key=$site_key&blog_key=$blog_key&blog_id=$blog_id&posting_id=$content_id&posting_title=".urlencode($content_title)."&posting_intro=".urlencode($content_intro)."&posting_url=".urlencode($content_url)."&blog_author_email=$content_author_email&plugin_version=$version&posting_tags=".urlencode($categories);
		}
		else
		{
			$context_title			= substr($this->platform_wrapper->sz_get_context_title($content), 0, $sz_context_title_length);
			$context_intro			= substr($this->platform_wrapper->sz_get_context_intro($content), 0, $sz_context_intro_length);
			$context_url			= $this->platform_wrapper->sz_get_context_url($content);
			$context_author_email	= $this->platform_wrapper->sz_get_context_author_email($content);
			
			$webservice_url	= "/webservices/ykwebservice_front.php?method=postContent&contentType=$content_type&site_key=$site_key&blog_key=$blog_key&blog_id=$blog_id&comment_id=$content_id&comment_title=".urlencode($content_title)."&comment_intro=".urlencode($content_intro)."&comment_url=".urlencode($content_url)."&email_address=".$content_author_email."&comment_author_url=".urlencode($content_author_url)."&posting_id=$context_id&posting_title=".urlencode($context_title)."&posting_intro=".urlencode($context_intro)."&posting_url=".urlencode($context_url)."&blog_author_email=".urlencode($context_author_email)."&plugin_version=$version&posting_tags=".urlencode($categories);
		}

		$response 				= $this->sz_functions_lib->sz_http_post_2_server('', $sz_server_url,$webservice_url , 80);
		$postcomment_ws_result 	= trim(substr($response,strpos($response,"CPRESPONSE")+10,strlen($response)));
		$result_arr				= $this->sz_functions_lib->sz_get_key_value_pairs_from_string_dictionary($postcomment_ws_result);

		if ('Y'==$result_arr["Success"])
		{ // The webservice call has been a success, hence insert/update the plugin schema

			// update sz_email table
			$sql = "SELECT COUNT(*) AS email_count
					FROM sz_email
					WHERE email_address = '$content_author_email'";
			$email_count = $this->platform_wrapper->sz_get_column_value_in_first_row_for_query($sql, 'email_count');
			if ($email_count == 1)
			{ // this email already exists, hence update it
				$sql = "UPDATE sz_email
						SET yk_score = '".$result_arr["YKScore"]."' , global_name = '".$result_arr["Global_Name"]."'
						WHERE email_address = '".$content_author_email."'" ;
			}
			else
			{ // this email does not exist, hence insert it
				$sql = "INSERT INTO sz_email (email_address, yk_score, global_name, encoded_email)
						VALUES ( '".$content_author_email."' , '".$result_arr["YKScore"]."' , '".$result_arr["Global_Name"]."', '".$result_arr["encoded_email"]."')" ;
			}
			$this->platform_wrapper->sz_execute_update_insert_query($sql);

			$yk_score 		= $result_arr["YKScore"];
			$raw_score		= ($yk_score-5)*10;
			$content_score	= $this->sz_functions_lib->sz_get_content_score($raw_score);

			$current_time=date('Y-m-d H:i:s');

			if ($yk_score != null)
			{
				$sql	= "	INSERT INTO sz_comment (blog_id, posting_id, comment_id, content_type, email_address, rating_count, comment_score,raw_score, creation_date)
							VALUES ($blog_id, $context_id, $content_id, '$content_type', '$content_author_email' , '0' , $content_score, $raw_score,'$current_time')";			
				/*
				switch ($sz_db_type)
				{
				case 'mysql':
				$sql	= "	INSERT INTO sz_comment (blog_id, posting_id, comment_id, email_address, rating_count, comment_score,raw_score, creation_date)
				VALUES ($blog_id, $context_id, $content_id, '$content_author_email' , '0' , $content_score, $raw_score, CURDATE())";
				break;
				case 'sqlite':
				$sql 	= "	INSERT INTO sz_comment (blog_id, posting_id, comment_id, email_address, rating_count, comment_score,raw_score, creation_date)
				VALUES ($blog_id, $context_id, $content_id, '$content_author_email' , '0' , $content_score, $raw_score, DATETIME('now'))";
				break;
				case 'oracle':
				$sql	= "	INSERT INTO sz_comment (blog_id, posting_id, comment_id, email_address, rating_count, comment_score,raw_score, creation_date)
				VALUES ($blog_id, $context_id, $content_id, '$content_author_email' , '0' , $content_score, $raw_score,to_char(current_timestamp, 'YYYY-MM-DD HH24:MI:SS'))";
				break;
				case 'firebird':
				$sql	= "	INSERT INTO sz_comment (blog_id, posting_id, comment_id, email_address, rating_count, comment_score,raw_score, creation_date)
				VALUES ($blog_id, $context_id, $content_id, '$content_author_email' , '0' , $content_score, $raw_score, CAST('NOW' AS TIMESTAMP))";
				break;
				case 'mssql':
				$sql	= "	INSERT INTO sz_comment (blog_id, posting_id, comment_id, email_address, rating_count, comment_score,raw_score, creation_date)
				VALUES ($blog_id, $context_id, $content_id, '$content_author_email' , '0' , $content_score, $raw_score, GETDATE())";
				break;
				case 'postgres':
				$sql	= "	INSERT INTO sz_comment (blog_id, posting_id, comment_id, email_address, rating_count, comment_score,raw_score, creation_date)
				VALUES ($blog_id, $context_id, $content_id, '$content_author_email' , '0' , $content_score, $raw_score, CURRENT_TIMESTAMP)";
				break;
				}
				*/
			}
			else
			{
				$sql	= "	INSERT INTO sz_comment (blog_id, posting_id, comment_id, content_type, email_address, creation_date)
							VALUES ($blog_id, $context_id, $content_id, '$content_type', '$content_author_email', $current_time )";
				/*
				switch ($sz_db_type)
				{
				case 'mysql':
				$sql 	= "	INSERT INTO sz_comment (blog_id, posting_id, comment_id, email_address, creation_date)
				VALUES ($blog_id, $context_id, $content_id, '$content_author_email', CURDATE() )";
				break;
				case 'sqlite':
				$sql 	= "	INSERT INTO sz_comment (blog_id, posting_id, comment_id, email_address, creation_date)
				VALUES ($blog_id, $context_id, $content_id, '$content_author_email', DATETIME('now') )";
				break;
				case 'oracle':
				$sql 	= "	INSERT INTO sz_comment (blog_id, posting_id, comment_id, email_address, creation_date)
				VALUES ($blog_id, $context_id, $content_id, '$content_author_email', TO_CHAR(current_timestamp, 'YYYY-MM-DD HH24:MI:SS') )";
				break;
				case 'firebird':
				$sql 	= "	INSERT INTO sz_comment (blog_id, posting_id, comment_id, email_address, creation_date)
				VALUES ($blog_id, $context_id, $content_id, '$content_author_email', CAST('NOW' AS TIMESTAMP) )";
				break;
				case 'mssql':
				$sql 	= "	INSERT INTO sz_comment (blog_id, posting_id, comment_id, email_address, creation_date)
				VALUES ($blog_id, $context_id, $content_id, '$content_author_email', GETDATE() )";
				break;
				case 'postgres':
				$sql 	= "	INSERT INTO sz_comment (blog_id, posting_id, comment_id, email_address, creation_date)
				VALUES ($blog_id, $context_id, $content_id, '$content_author_email', CURRENT_TIMESTAMP)";
				break;
				}
				*/
			}

		}
		else
		{ // status is blocked, insert only minimal data. If the comment is approved, the reverse websservice callback will happen to update the sz_comment table
			$sql 	= "	INSERT INTO sz_comment (blog_id, posting_id, comment_id, content_type, email_address, creation_date, exclude_flag)
						VALUES ($blog_id, $context_id, $content_id, '$content_type', '$content_author_email', '$current_time', 'B')";
			/*
			switch ($sz_db_type)
			{
			case 'mysql':
			$sql 	= "	INSERT INTO sz_comment (blog_id, posting_id, comment_id, email_address, creation_date, exclude_flag)
			VALUES ($blog_id, $context_id, $content_id, '$content_author_email', CURDATE(), 'B')";
			break;
			case 'sqlite':
			$sql 	= "	INSERT INTO sz_comment (blog_id, posting_id, comment_id, email_address, creation_date, exclude_flag)
			VALUES ($blog_id, $context_id, $content_id, '$content_author_email', DATETIME('now'), 'B')";
			break;
			case 'oracle':
			$sql	= "	INSERT INTO sz_comment (blog_id, posting_id, comment_id, email_address, creation_date, exclude_flag)
			VALUES ($blog_id, $context_id, $content_id, '$content_author_email', TO_CHAR(current_timestamp, 'YYYY-MM-DD HH24:MI:SS'), 'B')";
			break;
			case 'firebird':
			$sql 	= "	INSERT INTO sz_comment (blog_id, posting_id, comment_id, email_address, creation_date, exclude_flag)
			VALUES ($blog_id, $context_id, $content_id, '$content_author_email', CAST('NOW' AS TIMESTAMP), 'B')";
			break;
			case 'mssql':
			$sql 	= "	INSERT INTO sz_comment (blog_id, posting_id, comment_id, email_address, creation_date, exclude_flag)
			VALUES ($blog_id, $context_id, $content_id, '$content_author_email', GETDATE(), 'B')";
			break;
			case 'postgres':
			$sql 	= "	INSERT INTO sz_comment (blog_id, posting_id, comment_id, email_address, creation_date, exclude_flag)
			VALUES ($blog_id, $context_id, $content_id, '$content_author_email', CURRENT_TIMESTAMP, 'B')";
			break;
			}
			*/
		}
		$this->platform_wrapper->sz_execute_update_insert_query($sql);
		return '1';
	}



	/**
	* Returns the script that would fetch SezWho's JS, CSS and populate global configuration parameters.
	*
	* @return string header script
	*/
	function sz_get_header_script($blog_id=0)
	{
		return $this->sz_functions_lib->sz_get_header_script($blog_id);
	}
	

	/**
	* Returns script containing content configuration parameter information required by image, profile & rating/score bar script
	*
	* @param int $content_id
	* @return string content configuration-parameter script
	*/
	function sz_get_content_config_params_script(&$content)
	{
		return $this->sz_functions_lib->sz_get_content_config_params_script($content);
	}
	
	
	/**
	* Returns script for creating content author image
	*
	* @param int $content_id
	* @return string author image script
	*/
	function sz_get_content_author_image_script(&$content)
	{
		return $this->sz_functions_lib->sz_get_content_author_image_script($content);
	}
	

	/**
	* Returs script for creating profile link
	*
	* @param int $content_id
	* @return string profile link script
	*/
	function sz_get_content_author_profile_link_script(&$content)
	{
		return $this->sz_functions_lib->sz_get_content_author_profile_link_script($content);
	}
	
	
	/**
	* Returns script for creating rating/score bar
	*
	* @param int $content_id
	* @return string rating/score bar script
	*/
	function sz_get_content_rating_n_score_bar_script(&$content)
	{
		return $this->sz_functions_lib->sz_get_content_rating_n_score_bar_script($content);
	}

	
	/**
	* @ignore 
	*/
	function sz_get_rc_script()
	{

		global $sz_plugin_url, $sz_server_url;
		$blog_id = 0;

		$rc_config 			= $this->sz_get_rc_config();
		$rc_size 			= ($rc_config['sz_rc_size'])			? $rc_config['sz_rc_size']			: 5;
		$rc_width			= ($rc_config['sz_rc_width'])			? $rc_config['sz_rc_width']			: 160;
		$rc_color			= ($rc_config['sz_rc_color'])			? $rc_config['sz_rc_color']			: 'grey';
		$rc_image_width		= ($rc_config['sz_rc_image_width'])		? $rc_config['sz_rc_image_width']	: 40;
		$rc_image_height	= ($rc_config['sz_rc_image_height'])	? $rc_config['sz_rc_image_height']	: 40;

		$comment_filter_date 	= date('Y-m-d', time() - 30*24*3600) ; // 30 days
		$sql 					= $this->platform_wrapper->sz_get_query_2_fetch_rc_data($comment_filter_date, $rc_size);
		$commenters_data		= $this->platform_wrapper->sz_execute_select_query($sql);

		
		$rc_script 	= '<script type=\'text/javascript\'>';
		$rc_script 	.= 'var sz_rc_config_params = {sz_rc_data:[]};';
		$rc_script	.= '</script>';
		
		$rc_script 	.= '<table class="szRCEmbedTableClass" style="width:'. $rc_width .'px;">';
		$rc_script 	.= '<thead><tr><th colspan="2"><span id="szRCEmbedTitleID">Red Carpet</span></th></tr></thead>';
		$rc_script 	.= '<tfoot><tr><td colspan="2" class="szRCEmbedBrandingCell" align="center"><a class="cpEmbedPageTableCellBrandingLink" href="javascript:void SezWho.Utils.openPage(\'home\');"></a></td></tr></tfoot>';
		$rc_script 	.= '<tbody>';

		$rc_comments_info_arr='';
		$comment_iteration_num = 0;
		foreach ($commenters_data as $commenter_info )
		{
			
			$email_address 					= $commenter_info['email_address'] ;
			$md5_email_address 				= md5(strtolower($email_address)) ;
			$yk_score 						= $commenter_info['yk_score'] ;
			$global_name 					= $commenter_info['global_name'] ;
			$comment_author_url 			= $commenter_info['comment_author_url'] ;
			$comment_author 				= $commenter_info['comment_author'] ;
			$comment_id 					= $commenter_info['comment_id'] ;
			$enc_comment_author_email 		= $commenter_info['encoded_email'];
			
			$content						= array();
			$content['context_id']			= $commenter_info['comment_post_id'];
			$content['content_id']			= $commenter_info['comment_id'] ;
			$content['content_type']		= 'C';
			$comment_url 					= $this->platform_wrapper->sz_get_content_url($content);

			$image_address_url = $md5_email_address.'_t';
			$imageurl = 'http://s3.amazonaws.com/sz_users_images/'.$image_address_url;
			$noimgurl = 'http://s3.amazonaws.com/sz_users_images/noimg.gif';

			$rc_script 	.= '<tr><td>';
			$rc_script 	.= '<img style="width:'.$rc_image_width . 'px;height:' .$rc_image_height .'px;" class="szRCEmbedImage" src= "'.$imageurl .'" onerror=this.src="'.$noimgurl.'"></td>'; //</a></td>';
			$rc_script	.= '<td><a id="sz_profile_link:'.$comment_iteration_num.'" class="szEmbedCommeterName" onmouseover="javascript:SezWho.Utils.cpProfileRCEventHandler(event)" onmouseout="javascript:SezWho.DivUtils.cancelPopUp();">';
			$rc_script 	.= $comment_author .'</a><br />'.$this->sz_functions_lib->sz_get_star_rating($yk_score).'<br />';
			$rc_script 	.= '<a class="szEmbedCommetLink" onclick="javascript:SezWho.Utils.szClick(\'CC\',\''.$comment_url.'\',0,0)">Latest Comment</a></td>';
			$rc_script 	.= '</tr>	<tr><td colspan="2" class="szRCEmbedTableSeperator"></td></tr>';

			$rc_comments_info_arr .= 'sz_rc_config_params.sz_rc_data['.$comment_iteration_num.']= {comment_id:'.$comment_id.', comment_author: "'. $comment_author .'", comment_author_url: "'.$comment_author_url .'", comment_author_email: "'.$enc_comment_author_email. '",sz_score:0,comment_score:0};';
			$comment_iteration_num++;
		}

		$rc_script 	.= '</tbody></table>';
		$rc_script 	.= '<script type="text/javascript">';
		$rc_script 	.= 'if (!sz_global_config_params) var sz_global_config_params = {cpserverurl:"'.$sz_server_url.'"};';
		$rc_script	.= 'var sz_rc_config_params = {';
		$rc_script 	.= 'use_cross_domain_posting:1,cppluginurl:"'.$sz_plugin_url.'",cpserverurl:"'.$sz_server_url.'",sitekey: "'.$site_key.'",blog_key:"'.$blog_key.'",blog_id:"'.$blog_id .'",comment_number:0,sz_auto_comment:0,sz_auto_option_bar:0,sz_rc_data:[]};';
		$rc_script 	.= $rc_comments_info_arr. '</script>';
		return $rc_script;


	}


	/**
	* Sets red carpet configuration data
	*
	* @param array $rc_config
	*/
	function sz_set_rc_config($rc_config)
	{
		$this->platform_wrapper->sz_set_key_value('sz_rc_size',$rc_config['sz_rc_size']);
		$this->platform_wrapper->sz_set_key_value('sz_rc_width',$rc_config['sz_rc_width']);
		$this->platform_wrapper->sz_set_key_value('sz_rc_color',$rc_config['sz_rc_color']);
		$this->platform_wrapper->sz_set_key_value('sz_rc_image_width',$rc_config['sz_rc_image_width']);
		$this->platform_wrapper->sz_set_key_value('sz_rc_image_height',$rc_config['sz_rc_image_height']);
		
		return '1';
	}


	/**
	* Returns red carpet configuration data
	*
	* @return array
	*/
	function sz_get_rc_config()
	{
		$rc_config							= array();
		
		$rc_config['sz_rc_size']			= $this->platform_wrapper->sz_get_key_value("sz_rc_size");
		$rc_config['sz_rc_width']			= $this->platform_wrapper->sz_get_key_value("sz_rc_width");
		$rc_config['sz_rc_color']			= $this->platform_wrapper->sz_get_key_value("sz_rc_color");
		$rc_config['sz_rc_image_width']		= $this->platform_wrapper->sz_get_key_value("sz_rc_image_width");
		$rc_config['sz_rc_image_height']	= $this->platform_wrapper->sz_get_key_value("sz_rc_image_height");		

		return $rc_config;
	}


	/**
	* @ignore 
	*/
	function sz_get_badge_script()
	{
		$sz_number_of_badges	= $this->sz_get_no_of_badges();

		$badge_script	 = "<script type='text/javascript'>";
		$badge_script	.= 'var sz_badge_config_params = {sz_badge_data:[]};';
		$badge_script	.= "</script>";

		for($i=1; $i<=$sz_number_of_badges; $i++)
		{
			$email_address = $this->sz_get_email_for_badge_num($i);

			if ( empty($email_address) )
			{
				continue;
			}

			$sql		= $this->platform_wrapper->sz_get_query_2_fetch_badge_data($email_address);
			$badge_data	= $this->platform_wrapper->sz_execute_select_query($sql);

			if(count($badge_data)>0)
			{
				$md5_email_address 			= md5(strtolower($email_address));
				$yk_score 					= $badge_data[0]['yk_score'];
				$global_name 				= $badge_data[0]['global_name'];
				$comment_author 			= $badge_data[0]['comment_author'];
				$enc_comment_author_email 	= $badge_data[0]['encoded_email'];
				
				if($comment_author=='' || $comment_author==null)
				{
					continue;
				}

				$badge_script .= "<table class='szBadgeBody'>";
				$badge_script .=	"<tr><td class='szBadgeBodyFiller'></td></tr>";
				$badge_script .= "<tr><td class='szBadgeBodyDataName'><a class='szBadgeBodyDataNameLink' id='sz_badge_profile_link:$i' onmouseover='javascript:SezWho.Utils.cpProfileBadgeEventHandler(event)' onmousedown='javascript:SezWho.Utils.cpProfileBadgeEventHandler(event)' href='' onmouseout='javascript:SezWho.DivUtils.cancelPopUp();'>".$comment_author."</a></td></tr>";
				$badge_script .= "<tr><td class='szBadgeBodyDataSP'>Star Power:</td></tr>";
				$badge_script .= "<tr><td class='szBadgeBodyDataButtons'>". $this->sz_functions_lib->sz_get_star_rating($yk_score, 'badge')."</td></tr>";
				$badge_script .= "<tr><td class='szBadgeBodyLogo'><a class='szBadgeBodyLogoLink' href=\"javascript:void SezWho.Utils.openPage('home');\"></a></td></tr>";
				$badge_script .= "</table>";

				$badge_info_arr .= "sz_badge_config_params.sz_badge_data[".$i."]= {comment_author:'".rawurlencode($comment_author)."', comment_author_email:'".$enc_comment_author_email."'};";
			}
		}
		$badge_script .= "<script type='text/javascript'>" . $badge_info_arr . "</script>";
		
		return $badge_script;
	}

	
	/**
	* Sets number of badges
	*
	* @param int $no_of_badges
	*/
	function sz_set_no_of_badges($no_of_badges)
	{
		$this->platform_wrapper->sz_set_key_value("sz_no_of_badges",$no_of_badges);
	}

	
	/**
	* Returns number of badges
	*
	* @return string
	*/
	function sz_get_no_of_badges()
	{
		return $this->platform_wrapper->sz_get_key_value("sz_no_of_badges");
	}


	/**
	* Sets email for a particular badge (specified by badge number)
	*
	* @param int $badge_no
	* @param string $email
	*/
	function sz_set_email_for_badge_num($badge_no, $email)
	{
		$this->platform_wrapper->sz_set_key_value("sz_email_for_badge_no_$badge_no",$email);
	}

	/**
	* Returns email for a badge (specified by badge number)
	*
	* @param int $badge_no
	* @return string
	*/
	function sz_get_email_for_badge_num($badge_no)
	{
		return $this->platform_wrapper->sz_get_key_value("sz_email_for_badge_no_$badge_no");
	}
	
}
?>