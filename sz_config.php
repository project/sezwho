<?php

/**
* @package SezWho
* @copyright (c) 2008 sezwho.com
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/


global $sz_site_url;
//############################################################################################################################
//#################### Edit the $sz_site_url below to point it to your site ##################################################
$sz_site_url = 'YOUR DRUPAL SITE URL'; // (e.g. $sz_site_url = 'http://mysite.com' OR $sz_site_url = 'http://mysite.com/blog')
//############################################################################################################################


global $sz_enable_content_rating, $sz_enable_content_filter, $sz_enable_auto_layout,$sz_user_link_repo_use_enc, $sz_show_commenter_pic, $sz_auto_option_bar, $sz_auto_comment;

$sz_enable_content_rating	= 1;
$sz_enable_content_filter	= 0;
$sz_enable_auto_layout		= 1;
$sz_user_link_repo_use_enc	= 0;
$sz_show_commenter_pic 		= 0;
$sz_auto_option_bar			= 0;
$sz_auto_comment 			= 0;

?>