<?php

/**
* @package SezWho
* @copyright (c) 2008 sezwho.com
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/


global $sz_plugin_path;
$sz_plugin_path 	= dirname(__FILE__);

require_once($sz_plugin_path.'/sz_config.php');


global $sz_platform, $sz_plugin_url, $sz_db_type;

$sz_platform 		= 'DR';
$sz_plugin_url		= $sz_site_url.'/sites/all/modules/sezwho';

$sz_db_type			= '';//'<SET DATABASE TYPE: mysql,sqlite, oracle, firebird, postgres, mssql>';
switch ($GLOBALS['db_type'])
{
	case 'mysqli':
	case 'mysql':
		$sz_db_type	= 'mysql';
		break;
	case 'pgsql':
		$sz_db_type	= 'postgres';
		break;
}


require_once($sz_plugin_path.'/engine/sz_bootstrap.php');
?>